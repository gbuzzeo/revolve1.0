$(document).ready(function(){
    var isWebkit = 'webkitRequestAnimationFrame' in window;
    if(isWebkit){
    $('head').append("<link href='/RevolveApplication/css/chrome.css' type='text/css' rel='stylesheet' />");
    
    } 

});

    function changeGoogleTranslate() {
   
    if ($('.goog-te-combo option:first-child').text() == "Select Language") {
         
         var option = $('<option></option>').attr("value", "en").text("English");
         $(option).insertAfter('.goog-te-combo option:first-child');
         
   
    $('.goog-te-combo').selectBox().change(function () {
        
        
   
    var gObj = $('.goog-te-combo');
    var db = gObj.get(0);
    
    gObj.val($(this).val());
    fireEvent(db, 'change');
    });
    } else {
    setTimeout(changeGoogleTranslate, 50);
    }
    }
    changeGoogleTranslate();
    function fireEvent(el,e){
    if (document.createEventObject){
    //for IE
    var evt = document.createEventObject();
    return el.fireEvent('on'+e,evt)
    }
    else{
    // For other browsers
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent(e, true, true );
    return !el.dispatchEvent(evt);
    }
    }


(function($) {
   
    $.fn.changeElementType = function(newType) {
        var attrs = {};
        if (!(this[0] && this[0].attributes))
            return;

        $.each(this[0].attributes, function(idx, attr) {
            attrs[attr.nodeName] = attr.nodeValue;
        });
        this.replaceWith(function() {
            return $("<" + newType + "/>", attrs).append($(this).contents());
        });
    }
})(jQuery);

$('.view-row').changeElementType('li');
$('.view-content').changeElementType('ul');
$('.view-organigramme-departements').wrap('<ul />').changeElementType('li');
