function sendInfoToCcIframe(key,info)
{
    var win = window.frames["PID_S"+key];            
    win.postMessage(info, "https://23.21.96.159");
                
}

            
function listenForInfoFromIframe(event)
{
    if (event.origin !== "https://23.21.96.159")
        return;

    ret = event.data;
    two = ret.split('***');
    three = two[1].split("|");
    four = three[1].split(":");
    key = four[1];
    if (two[2].substr(0,6) === 'token:')
    {
        five = two[2].split(":");
        e1 = document.getElementById('cc_token_' + key);
        e1.value = five[1];
        e1.focus();
        document.activeElement.blur();
    }
    else
    {
        document.getElementById("cc_back_from_child_" + key).value = ret;
    }
    session_id = document.getElementById("cc_session_id_" + key).value;
    entity_id = document.getElementById("cc_entity_id_" + key).value;
    fname = document.getElementById("cc_fname_" + key).value;
    lname = document.getElementById("cc_lname_" + key).value;
    addr1 = document.getElementById("cc_addr1_" + key).value;
    addr2 = document.getElementById("cc_addr2_" + key).value;
    city = document.getElementById("cc_city_" + key).value;
    state = document.getElementById("cc_state_" + key).value;
    zip = document.getElementById("cc_zip_" + key).value;

    sendInfoToCcIframe(key, "entity_id:" + entity_id + "|session_id:" + session_id + '|first_name:' + fname + '|last_name:' + lname + '|addr1:' + addr1 + '|addr2:' + addr2 + '|city:' + city + '|state:' + state + '|zip:' + zip);
}

window.addEventListener("DOMContentLoaded", function() {
    window.addEventListener("message", listenForInfoFromIframe, false);
});
