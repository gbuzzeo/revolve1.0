
package com.revolve.main;

import java.io.File;
import com.vaadin.server.VaadinService;
import com.revolve.RevolveUI;
import com.vaadin.server.ThemeResource;
import java.io.Serializable;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WebBrowser;
import org.vaadin.cssinject.CSSInject;
import java.util.Properties;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Embedded;

public class SplashPage implements Serializable {
    Embedded splash_oms_logo;
    Embedded static_swoops_top;
    String splashLogoTop = "50px";
    String splashSeperatorTop = "60px";
    String splashSeperator1Top = "440px";
    String splashBottomStarsTop = "465px";
    String splashLogoLeft = "60px";
    
    public SplashPage(AbsoluteLayout abs) {
        /**
         * Load the client properties from the session
         */
        Properties clientProperties = (Properties) VaadinSession.getCurrent().getAttribute("ClientProperties");
        
        /**
         * Load the Revolve Image on the Splash
         */
        splash_oms_logo = new Embedded(null, new ThemeResource("clientImages/" + clientProperties.getProperty("logo")));
        
        /**
         * Load the swoops at top and bottom of Splash
         */
        static_swoops_top = new Embedded(null, new ThemeResource("images/SWOOP_REV.png"));
        abs.addComponent(static_swoops_top,"right:0px;top:0px");
 
        /**
         * If the client properties are set to show the logo then place the client logo on the splash page
         * otherwise show the revolve logo
         */
        boolean showLogo = true;
        if(clientProperties.getProperty("logo").equals("no")) {
            showLogo = false;
        }
        if(showLogo==true) {
            abs.addComponent(splash_oms_logo,"left:" + splashLogoLeft + ";top:" + splashLogoTop);
        }else{
            Embedded revolveLogo = new Embedded(null,new ThemeResource("clientImages/revolveLogo.png"));
            revolveLogo.setWidth("400px");
            abs.addComponent(revolveLogo,"left:" + splashLogoLeft + ";top:" + splashLogoTop);
        }
    }
    
}
