package com.revolve.main;

import com.google.common.eventbus.Subscribe;
import com.vaadin.ui.AbsoluteLayout;
import com.revolve.eventbus.events.RemoveSplashPageEvent;
import com.revolve.eventbus.events.WindowLoadEvent;
import com.revolve.eventbus.events.LoadSystemFrameHeaderEvent;
import com.revolve.eventbus.RevolveEventBus;
import com.revolve.eventbus.events.LoadOptionOneIconsEvent;
import com.revolve.eventbus.events.CreateToolBoxEvent;

public class CreateMainLayout {
    private AbsoluteLayout abs = new AbsoluteLayout();
    
    public CreateMainLayout() {
        RevolveEventBus.register(this);
    }
    /**
     * This is the Main Layout for the application 
     */
    public AbsoluteLayout getLayout() {
        return abs;
    }
    
     /**
     * Subscribe for remove splash page event
     */
    @Subscribe
    public void removeSplashPageEvent(final RemoveSplashPageEvent event) {
        abs.removeAllComponents();
    }
     
    /**
     * Subscribe for System Frame Header Event
     */
    @Subscribe
    public void loadSystemFrameHeaderEvent(final LoadSystemFrameHeaderEvent event) {
        try {
            abs.addComponent(event.getHeader(),"top:0px;left:0px;right:0px;");
        }
        catch(Exception e) {         
        }
    }
    
    /**
     * Subscribe for Option One Icons Event
     */
    @Subscribe
    public void loadOptionOneIconsEvent(final LoadOptionOneIconsEvent event) {
        try {
            abs.addComponent(event.getVerticalLayout(),"top:50px;left:100px;right:0px;bottom:150px");
        }
        catch(Exception e) {     
            e.printStackTrace();
        }
    }   
    
    /**
     * Subscribe for Toolbox on left side
     */
    @Subscribe
    public void CreateToolBoxEvent(final CreateToolBoxEvent event) {
        try {
            abs.addComponent(event.getToolboxComponent(),"top:120px;left:0px;bottom:40px;");
        }
        catch(Exception e) {     
            e.printStackTrace();
        }
    }   
}
