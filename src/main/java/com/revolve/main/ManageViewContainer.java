package com.revolve.main;

import java.util.Date;
import java.util.Iterator;
import java.util.Collection;
import java.io.Serializable;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Component;
import com.vaadin.data.Item;
import com.vaadin.ui.TextField;
import com.vaadin.ui.DateField;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.PopupDateField;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

public class ManageViewContainer  implements Serializable {
    private IndexedContainer vc = new IndexedContainer();
    
    public ManageViewContainer() {
    }

    public void clearContainer() {
        vc.removeAllItems();
    }

    public void buildViewContainer() {
        vc.addContainerProperty("Value", String.class, null);
        vc.addContainerProperty("Id", String.class, null);
        vc.addContainerProperty("cbValue", Boolean.class, null);
        vc.addContainerProperty("ValueBefore", String.class, null);
        vc.addContainerProperty("cbValueBefore", Boolean.class, null);
        vc.addContainerProperty("dateValue", Date.class, null);
        vc.addContainerProperty("xParentId", IndexedContainer.class, null);
        vc.addContainerProperty("FieldName", String.class, null);
        vc.addContainerProperty("FieldCaption", String.class, null);
        vc.addContainerProperty("FieldPanel", String.class, null);
        vc.addContainerProperty("FieldLine", String.class, null);
        vc.addContainerProperty("FieldOrder", Integer.class, null);
        vc.addContainerProperty("FieldType", String.class, null);
        vc.addContainerProperty("FieldObject", String.class, null);
        vc.addContainerProperty("FieldExpandRatio", Integer.class, null);
        vc.addContainerProperty("FieldFormat", String.class, null);
        vc.addContainerProperty("FieldDataType", String.class, null);
        vc.addContainerProperty("FieldReadOnly", String.class, null);
        vc.addContainerProperty("FieldRequired", Boolean.class, null);
        vc.addContainerProperty("FieldGroup", String.class, null);
        vc.addContainerProperty("TableName", String.class, null);
        vc.addContainerProperty("WidgetContainer", Object.class, null);       
        vc.addContainerProperty("WidgetName", String.class, null);       
        vc.addContainerProperty("WidgetContainer", IndexedContainer.class, null);       
        vc.addContainerProperty("ParentInfo",String.class, null);
        vc.addContainerProperty("ValueChanged",Boolean.class, null);
        
    }
    
    public IndexedContainer getContainer() {
        return vc;
    }
    
    public void setContainer(IndexedContainer icc) {
        vc.removeAllItems();
        vc = copyContainer(icc);
    }
    
    public Component loadItem(Component c, String object, String fieldName, String tableName, Boolean required) {
        if(c.getClass().toString().equals("class com.vaadin.ui.Button")) {
            return c;
        }
        boolean itemExists = true;
        Item newItem = null;
        try {
            newItem = vc.getItem(object + "_" + fieldName);
            if(newItem==null)
                itemExists = false;
        }
        catch(Exception e) {
        itemExists = false;
        }
        if(itemExists==false) {
            newItem = vc.addItem(object + "_" + fieldName);
            newItem.getItemProperty("Value").setValue("");
            newItem.getItemProperty("FieldRequired").setValue(required);
             try {
                newItem.getItemProperty("cbValue").setValue(false);
                newItem.getItemProperty("FieldName").setValue(fieldName);
                newItem.getItemProperty("TableName").setValue(tableName);
            }
            catch(Exception e) {
                
            }
        }
        Component component = null;
        
        if(c.getClass().toString().equals("class com.vaadin.ui.TextField")) {
            final TextField textField = (TextField) c;
            textField.setId(object + "_" + fieldName);
            textField.setRequired(required);
            textField.setPropertyDataSource(vc.getContainerProperty(object + "_" + fieldName, "Value"));
            component = textField;
            newItem.getItemProperty("FieldType").setValue("TextField");
            textField.setImmediate(true);
            textField.addValueChangeListener(new Property.ValueChangeListener() {
                public void valueChange(ValueChangeEvent event) {
                    vc.getContainerProperty(textField.getId(), "ValueChanged").setValue(!textField.getValue().toString().isEmpty());
                }
            });
         }
         if(c.getClass().toString().equals("class com.vaadin.ui.DateField")) {
            final DateField textField = (DateField) c;
            textField.setId(object + "_" + fieldName);
            textField.setRequired(required);
            textField.setPropertyDataSource(vc.getContainerProperty(object + "_" + fieldName, "dateValue"));
            component = textField;
            newItem.getItemProperty("FieldType").setValue("DateField");
            textField.setImmediate(true);
        }
         
        if(c.getClass().toString().equals("class com.vaadin.ui.PopupDateField")) {
            final PopupDateField textField = (PopupDateField) c;
            textField.setReadOnly(false);
            textField.setId(object + "_" + fieldName);
            textField.setRequired(required);
            textField.setReadOnly(true);
            textField.setPropertyDataSource(vc.getContainerProperty(object + "_" + fieldName, "dateValue"));
            component = textField;
            newItem.getItemProperty("FieldType").setValue("DateField");
            textField.setImmediate(true);
            textField.addValueChangeListener(new Property.ValueChangeListener() {
                public void valueChange(ValueChangeEvent event) {
                    try {
                        vc.getContainerProperty(textField.getId(), "ValueChanged").setValue(!textField.getValue().toString().isEmpty());
                    }
                    catch(Exception e) {
                    }
               }
            });
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.TextArea")) {
            final TextArea textArea = (TextArea) c;
            textArea.setId(object + "_" + fieldName);
            textArea.setRequired(required);
            //textArea.setReadOnly(true);
            textArea.setPropertyDataSource(vc.getContainerProperty(object + "_" + fieldName, "Value"));
            component = textArea;
            newItem.getItemProperty("FieldType").setValue("TextArea");
            textArea.setImmediate(true);
            textArea.addValueChangeListener(new Property.ValueChangeListener() {
                public void valueChange(ValueChangeEvent event) {
                     vc.getContainerProperty(textArea.getId(), "ValueChanged").setValue(!textArea.getValue().toString().isEmpty());
              }
            });
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.MaskedTextField")) {
            final TextField textField = (TextField) c;
            textField.setRequired(required);
            textField.setId(object + "_" + fieldName);
            textField.setPropertyDataSource(vc.getContainerProperty(object + "_" + fieldName, "Value"));
            component = textField;
            newItem.getItemProperty("FieldType").setValue("TextField");
            textField.setImmediate(true);
            textField.addValueChangeListener(new Property.ValueChangeListener() {
                public void valueChange(ValueChangeEvent event) {
                    vc.getContainerProperty(textField.getId(), "ValueChanged").setValue(!textField.getValue().toString().isEmpty());
                }
            });
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.PasswordField")) {
            final PasswordField passwordField = (PasswordField) c;
            passwordField.setRequired(required);
            passwordField.setId(object + "_" + fieldName);
            passwordField.setPropertyDataSource(vc.getContainerProperty(object + "_" + fieldName, "Value"));
            component = passwordField;
            newItem.getItemProperty("FieldType").setValue("TextField");
            passwordField.setImmediate(true);
            passwordField.addValueChangeListener(new Property.ValueChangeListener() {
                public void valueChange(ValueChangeEvent event) {
                    vc.getContainerProperty(passwordField.getId(), "ValueChanged").setValue(!passwordField.getValue().toString().isEmpty());
               }
            });
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.ComboBox")) {
            final ComboBox comboBox = (ComboBox) c; 
            comboBox.setNullSelectionAllowed(false);
            comboBox.setId(object + "_" + fieldName);
            /**
             * Check to see if there is a vlist for this field
             */
 //           try {
 //               String key = transactionContainer.getSystemName(object, fieldName);
 //               ArrayList<String> data = transactionContainer.getVlists(key);
 //               for(int q=0;q<data.size();q++) {
 //                   comboBox.addItem(data.get(q));
 //               }
 //           }
 //           catch(Exception e) {
 //           }
            comboBox.addItem("");
            comboBox.setValue("");
            comboBox.setRequired(required);
            comboBox.setPropertyDataSource(vc.getContainerProperty(object + "_" + fieldName, "Value"));
            component = comboBox;
            newItem.getItemProperty("FieldType").setValue("ComboBox");

            comboBox.setImmediate(true);
            comboBox.addValueChangeListener(new Property.ValueChangeListener() {
              public void valueChange(ValueChangeEvent event) {
                    try {
                        vc.getContainerProperty(comboBox.getId(), "ValueChanged").setValue(!comboBox.getValue().toString().isEmpty());
                    }
                    catch(Exception e) {
                        vc.getContainerProperty(comboBox.getId(), "ValueChanged").setValue(false);                       
                    }
                }
            });
        }
 
        if(c.getClass().toString().equals("class com.vaadin.ui.CheckBox")) {
            final CheckBox checkBox = (CheckBox) c;
            checkBox.setRequired(required);
            checkBox.setId(object + "_" + fieldName);
            checkBox.setPropertyDataSource(vc.getContainerProperty(object + "_" + fieldName, "cbValue"));
            component = checkBox;
            newItem.getItemProperty("FieldType").setValue("CheckBox");
            checkBox.setImmediate(true);
            checkBox.addValueChangeListener(new Property.ValueChangeListener() {
                public void valueChange(ValueChangeEvent event) {
                    vc.getContainerProperty(checkBox.getId(), "ValueChanged").setValue(checkBox.booleanValue());
                }
            });
        }
        
        return component;
   }
    
   public void loadWidget(String widgetName) {
       
   }
    
   public void loadViewData() {
   }
   
   public void setTransaction(String module, String task) {
   }
      
   public IndexedContainer copyContainer(IndexedContainer source) {
        IndexedContainer cont = new IndexedContainer();
        
        for (Object prop : source.getContainerPropertyIds())
            cont.addContainerProperty(prop, source.getType(prop), null);
        
        for (Object id : source.getItemIds()) {
            Item sourceItem = source.getItem(id);
            Item destItem = cont.addItem(id);
            for (Object prop : source.getContainerPropertyIds()) {
                Object value = sourceItem.getItemProperty(prop).getValue();
                destItem.getItemProperty(prop).setValue(value);
            }
        }
        
        return cont;
    }
   
    public boolean CheckForDataChanges() {
        boolean dataChanged = false;
        Collection c = vc.getItemIds();
        Iterator it = c.iterator();
        while(it.hasNext()) {
            Item item = vc.getItem(it.next());
            boolean ver = (Boolean) item.getItemProperty("ValueChanged").getValue();
            if(ver==true) {
                return ver;
            }
        }
        
        return dataChanged;
    }
    
    public void ClearChanges() {
        boolean dataChanged = false;
        Collection c = vc.getItemIds();
        Iterator it = c.iterator();
        while(it.hasNext()) {
            Item item = vc.getItem(it.next());
            item.getItemProperty("ValueChanged").setValue(false);
         }
        
     }
       
}
