package com.revolve.main;

import com.revolve.RevolveUI;
import com.google.common.eventbus.Subscribe;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import java.util.*;
import java.io.InputStream;
import com.vaadin.ui.Window;
import com.vaadin.ui.Notification;
import com.revolve.eventbus.RevolveEventBus;
import com.revolve.eventbus.events.DisplayMessageEvent;

public class ApplicationMessages {

    public static Properties properties = new Properties();
    public String[][] formCache = new String[500][2];
    public int formCacheCounter = 0;
    
    public ApplicationMessages() {
        RevolveEventBus.register(this);
    }

    public void loadMessages() {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("../../RevolveMessages.properties");
        try {
            properties.load(inputStream);
            Enumeration em = properties.keys();
            while (em.hasMoreElements()) {
                String str = (String) em.nextElement();
                formCache[formCacheCounter][0] = str;
                formCache[formCacheCounter][1] = properties.getProperty(str);
                formCacheCounter = formCacheCounter + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayMessage(String messageNo) {
        String value[] = properties.getProperty(messageNo).split("\\|");

        if (value[1].equals("1")) {
            Notification err = new Notification(messageNo, value[0]);
            err.setDelayMsec(2);
            Notification.show(value[0],Notification.Type.WARNING_MESSAGE);
        }
        if (value[1].equals("2")) {
            Notification err = new Notification(messageNo, value[0]);
            err.setDelayMsec(2);
            Notification.show(value[0],Notification.Type.WARNING_MESSAGE);
        }
        if (value[1].equals("3")) {

            final Window warningBox=new Window();
            VerticalLayout wbLayout = new VerticalLayout();
            warningBox.setContent(wbLayout);
            warningBox.setStyleName("warningBox");
            warningBox.setVisible(true);
            warningBox.setClosable(false);
            warningBox.setResizable(false);
            warningBox.setModal(true);
            warningBox.setWidth("400px");
            warningBox.setHeight("200px");
            warningBox.setCaption(messageNo);
            //warningBox.setBorder(2);
            Label warningTxt=new Label(value[0]);
            wbLayout.addComponent(warningTxt);
            Button okBtn=new Button("OK");
            okBtn.setStyleName("warningButton");
            okBtn.setImmediate(true);
            okBtn.addListener(new Button.ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                   RevolveUI.getCurrent().getUI().removeWindow(warningBox);
                }
            });
            wbLayout.addComponent(okBtn);
                RevolveUI.getCurrent().getUI().addWindow(warningBox);
           
        }
    }
    
    @Subscribe
    public void displayMessageEvent(final DisplayMessageEvent event) {
        displayMessage(event.getMessageNumber());
    }
}
