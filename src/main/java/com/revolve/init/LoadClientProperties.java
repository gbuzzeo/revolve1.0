package com.revolve.init;

import com.revolve.RevolveUI;
import com.revolve.events.resize.ManageWindows;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import com.vaadin.server.VaadinSession;
import org.vaadin.cssinject.CSSInject;

public class LoadClientProperties {
    private Properties clientProperties = new Properties();
    private CSSInject splashZoom;

    public void loadProperties(String clientName) {
        /**
         * Load the client properties file and store in session for later use.
         */
        String directory = System.getProperty("catalina.base");
        String clientDirectory = directory + File.separator + "clients" + File.separator + clientName + File.separator;
        try {
            InputStream inputStream = new FileInputStream(directory + File.separator + "clients" + File.separator + clientName + File.separator + "properties.txt");
            clientProperties.load(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * Set CSS for client specific setting and browser size
         */
        
        splashZoom = new CSSInject(RevolveUI.getCurrent().getUI());
        splashZoom.setStyles(".v-absolutelayout-splash" + "{-webkit-transform: scale(" + "1.2" + "," + "1.2" + ");}");

        CSSInject css2 = new CSSInject(RevolveUI.getCurrent().getUI());
        css2.setStyles(".mytheme.v-app, .mytheme.v-app-loading{background-color: " + clientProperties.getProperty("background") + ";}");

        CSSInject css3 = new CSSInject(RevolveUI.getCurrent().getUI());
        css3.setStyles(".mytheme .v-window-header{color: " + clientProperties.getProperty("background") + ";font-weight:900;font-size:18px;}");
   
        CSSInject css4 = new CSSInject(RevolveUI.getCurrent().getUI());
        css4.setStyles(".mytheme .v-panel-caption{zoom:" + 
                "-webkit-box-sizing: border-box;" +
                "-moz-box-sizing: border-box;" +
                "box-sizing: border-box;" +
                "padding: 0 12px;" +
                "line-height: 36px;" +
                "border-bottom: 1px solid #d5d5d5;" +
                "background-color:" + clientProperties.getProperty("background") + ";" +
                "background-image: linear-gradient(to bottom," + clientProperties.getProperty("background") + " 2%, " + clientProperties.getProperty("background") + " 98%);" +
                "color: #464646;" +
                "font-weight: 400;" +
                "font-size: 14px;" +
                "-webkit-box-shadow: inset 0 1px 0 white, inset 0 -1px 0 #eeeeee;" +
                "box-shadow: inset 0 1px 0 white, inset 0 -1px 0 #eeeeee;" +
                "text-shadow: 0 1px 0 rgba(255, 255, 255, 0.05);" +
                "border-radius: 3px 3px 0 0;}");
        
        CSSInject css5 = new CSSInject(RevolveUI.getCurrent().getUI());
        css5.setStyles(".mytheme .v-accordion{" +
            "background: white;" +
            "backgound-image: ;" +
            "border-radius: 4px;" +
            "border: 1px solid #d5d5d5;" +
            "-webkit-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.05);" +
            "color: #464646;" +
            "box-shadow: 0 2px 3px rgba(0, 0, 0, 0.05);" +
            "background-color:" + clientProperties.getProperty("background") + ";" +
            "overflow: hidden;}");
        
        CSSInject css6 = new CSSInject(RevolveUI.getCurrent().getUI());
        css6.setStyles(".v-table-row:hover, .v-table-row-odd:hover{" +
            "color: #464646;" +
            "background:" + clientProperties.getProperty("background") + ";}");
 
        VaadinSession.getCurrent().setAttribute("ClientProperties", clientProperties);
    }
}
