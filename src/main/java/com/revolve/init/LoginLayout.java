package com.revolve.init;

import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Button;
import com.vaadin.ui.themes.ValoTheme;
import com.revolve.eventbus.RevolveEventBus;
import com.revolve.login.LoginAssistant;
import com.revolve.eventbus.events.DisplayMessageEvent;
import com.revolve.eventbus.events.UserLoginEvent;
import com.vaadin.event.ShortcutAction;
import com.revolve.eventbus.EventPublisher;

public class LoginLayout {
    AbsoluteLayout loginLayout = new AbsoluteLayout();
    TextField username = new TextField("Username");
    PasswordField password = new PasswordField("Password");
    
    public LoginLayout() {
        RevolveEventBus.register(this);
    }

    public AbsoluteLayout getLoginLayout() {
        /**
         * Load the layout with login components
         */
        loginLayout.setWidth("325px");
        loginLayout.setHeight("500px");
        Label lang = new Label("<div id=\"google_translate_element\"></div>", Label.CONTENT_XHTML);
        lang.setImmediate(true);
        lang.setWidth("200px");
        loginLayout.addComponent(lang, "left: 40px; top: 40px;");
        loginLayout.addComponent(username, "left: 40px; top: 85px;");
        loginLayout.addComponent(password, "left: 40px; top: 155px;");
        Button loginSubmit = new Button();
        loginSubmit.setCaption("LET'S GO!");
        loginSubmit.setImmediate(true);
        loginSubmit.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        loginSubmit.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                boolean loginError = false;
                /**
                 * post to message event if username or password is incorrect
                 */
                if (username.getValue().toString().equals("")) {
                    EventPublisher.displayMessageEvent("AP0001");
                    return;
                }
                if (password.getValue().toString().equals("")) {
                    EventPublisher.displayMessageEvent("AP0002");
                    return;
                }
                
                /**
                 * Call the LoginEvent to verify the login credentials
                 */
                EventPublisher.userLoginEvent(username.getValue().toString(),password.getValue().toString());
           }
        });

        loginLayout.addComponent(loginSubmit, "left: 40px; top: 225px;");
        Button troubleLoggingIn = new Button("Having trouble logging in?");
        troubleLoggingIn.setStyleName(ValoTheme.BUTTON_LINK);
        troubleLoggingIn.addStyleName("login");
        troubleLoggingIn.setWidth("400px");
        troubleLoggingIn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                LoginAssistant loginAssistant = new LoginAssistant();
                loginAssistant.showAssistant();
            }
        });

        loginLayout.addComponent(troubleLoggingIn, "left: -60px; top: 265px;");

        return loginLayout;
    }

}
