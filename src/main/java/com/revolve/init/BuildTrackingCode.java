package com.revolve.init;

import com.revolve.json.JSONServices;
import com.vaadin.server.VaadinRequest;
import java.io.BufferedReader;
import java.io.FileReader;
import org.json.JSONObject;

public class BuildTrackingCode {
    String requestQueryString = "";

    public void build(VaadinRequest request, String clientID, String sessionID) {
        StringBuffer machineId = new StringBuffer();
        /*
            get the machine we are on
        */
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("machine.txt"));
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                machineId.append(line);
            }
        } catch (Exception e) {
        }

        JSONObject landingInfo = new JSONObject();
        try {
            // landingInfo.put("localAddr", request.getLocalAddr());
            // landingInfo.put("localPort", request.getLocalPort());
            landingInfo.put("locale", request.getLocale());
            landingInfo.put("remoteAddr", request.getRemoteAddr());
            landingInfo.put("remotePort", request.getRemotePort());
            landingInfo.put("remoteHost", request.getRemoteHost());
            landingInfo.put("remoteUser", request.getRemoteUser());
            //landingInfo.put("serverName", request.getServerName());
            //landingInfo.put("serverPort", request.getServerPort());
        } catch (Exception e) {
        }

        JSONObject object = new JSONObject();
        JSONObject tracking = new JSONObject();
        try {
            tracking.put("MachineNo", machineId);
            tracking.put("QueryString", requestQueryString);
            tracking.put("LandingSession", sessionID);
            tracking.put("LandingInfo", landingInfo.toString());
        } catch (Exception e) {
        }

        JSONObject sendingObject = new JSONObject();
        JSONObject TRANS = new JSONObject();
        JSONObject PARAMS = new JSONObject();
        try {
            TRANS.put("client_id", "2");
            TRANS.put("module", "system");
            TRANS.put("task", "register_hit");
            TRANS.put("user_id", clientID);
            TRANS.put("password", "test");
            TRANS.put("pl_session", sessionID);
            sendingObject.put("TRANS", TRANS);
            object.put("TrackingCode", tracking);
            PARAMS.put("0", object);
            sendingObject.put("PARAMS", PARAMS);
        } catch (Exception e) {
        }

        JSONServices jsonServices = new JSONServices();
        jsonServices.addRecord(sendingObject);        
    }
}
