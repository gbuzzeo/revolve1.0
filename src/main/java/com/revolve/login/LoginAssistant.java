package com.revolve.login;

import com.revolve.RevolveUI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.CheckBox;
import com.vaadin.data.Property;
import org.vaadin.jouni.animator.shared.AnimType;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Alignment;

public class LoginAssistant {
    Window window = new Window("Login Assistant");
    VerticalLayout windowLayout = new VerticalLayout();
    TextField username = new TextField("Username");
    TextField email = new TextField();
    TextField securityCode = new TextField("Security Code");
    TextField firstName = new TextField();
    TextField lastName = new TextField();
    Label fuLabel = new Label("Forgot your username?");
    CheckBox forgotUsername = new CheckBox("I forgot my username");
    CheckBox forgotPassword = new CheckBox("I forgot my password");
    Label forgotUsernameLabel = new Label("To reset your password, enter the username you use to sign in.");
    VerticalLayout workArea = new VerticalLayout();
    
    public void showAssistant() {
        window.setHeight("400px");
        window.setWidth("400px");
        window.setContent(windowLayout);
        window.setResizable(false);
        windowLayout.setMargin(true);
        windowLayout.setSpacing(true);
        workArea.setSizeFull();
        window.setClosable(false);
        windowLayout.addComponent(workArea);
        workArea.setSizeFull();
        workArea.addComponent(stepOne());
  
        RevolveUI.getCurrent().getUI().addWindow(window);
        window.center();
    }
    
    VerticalLayout stepOne() {
       workArea.removeAllComponents();
       forgotUsername.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if(forgotUsername.getValue()==true) {
                    workArea.addComponent(forgotUsernameLayout());
                }
            }
        });        
       forgotPassword.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if(forgotPassword.getValue()==true) {
                    workArea.addComponent(forgotPasswordLayout());
                }
            }
        });        
        VerticalLayout step = new VerticalLayout();
        step.setSpacing(true);
        step.addComponent(forgotUsername);
        step.addComponent(forgotPassword);
        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(window);
            }
        });
        step.addComponent(close);
        return step;
    }
    
    VerticalLayout forgotPasswordLayout() {
        workArea.removeAllComponents();
        VerticalLayout pi = new VerticalLayout();
        workArea.addComponent(pi);
        pi.setSizeFull();
        pi.setMargin(true);
        pi.setSpacing(true);
        forgotUsernameLabel.setStyleName(ValoTheme.LABEL_H4);
        Label unameLbl=new Label("Enter your Username");
        pi.addComponent(unameLbl);
        pi.addComponent(username);
        Button backButton = new Button("Back");
        backButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                workArea.addComponent(stepOne());
            }
        });

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(window);
            }
        });

        Button submit = new Button("Submit");
        submit.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
            }
        });

        buttons.setMargin(true);
        buttons.addComponent(close);
 //       buttons.setComponentAlignment(backButton, Alignment.BOTTOM_RIGHT);
        buttons.addComponent(backButton);
        buttons.setComponentAlignment(backButton, Alignment.BOTTOM_RIGHT);
        buttons.setExpandRatio(backButton,1);
        buttons.addComponent(submit);
        buttons.setComponentAlignment(submit, Alignment.BOTTOM_RIGHT);
        pi.addComponent(buttons);
        pi.setExpandRatio(buttons, 1);
        buttons.setWidth("100%");
        
        return pi;
    }
      
    VerticalLayout forgotUsernameLayout() {
        workArea.removeAllComponents();
        VerticalLayout forgot = new VerticalLayout();
        workArea.addComponent(forgot);
        forgot.setSizeFull();
        forgot.setMargin(true);
        forgot.setSpacing(true);
        fuLabel.setStyleName(ValoTheme.LABEL_H4);
        //forgot.setHeight("0px");
        forgot.setWidth("100%");
        Label emailLbl=new Label("Enter your email address on file");
        forgot.addComponent(emailLbl);
        forgot.addComponent(email);
        Label nameLbl=new Label("Enter the name on account");
        forgot.addComponent(nameLbl);
        HorizontalLayout h = new HorizontalLayout();
        h.setWidth("100%");
        firstName.setInputPrompt("First Name");
        lastName.setInputPrompt("Last Name");
        h.addComponent(firstName);
        h.addComponent(lastName);
        forgot.addComponent(h);
        //forgot.addComponent(securityQuestions);
        //forgot.addComponent(securityAnswer);
        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(window);
            }
        });

        Button backButton = new Button("Back");
        backButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                workArea.addComponent(stepOne());
            }
        });

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button submit = new Button("Submit");
        submit.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
            }
        });

        buttons.setMargin(true);
        buttons.addComponent(close);
        buttons.addComponent(backButton);
        buttons.setComponentAlignment(backButton, Alignment.BOTTOM_RIGHT);
        buttons.setExpandRatio(backButton,1);
        buttons.addComponent(submit);
        buttons.setComponentAlignment(submit, Alignment.BOTTOM_RIGHT);
        forgot.addComponent(buttons);
        forgot.setExpandRatio(buttons, 1);
        buttons.setWidth("100%");
      
        return forgot;
    }
    
}
