
package com.revolve.components;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Component;
import com.vaadin.server.Responsive;
import com.vaadin.ui.VerticalLayout;

public class RevolveResponsiveLine {

    public HorizontalLayout getLine(VerticalLayout layout, int layoutRatio, Component[] components, int[] width) {
        HorizontalLayout line = new HorizontalLayout();
        line.setSpacing(true);
        for(int a=0;a<components.length;a++) {
            Component c = components[a];
            c.setWidth("100%");
            line.addComponent(c);
            line.setExpandRatio(c,width[a]);
            Responsive.makeResponsive(c);
        }
        Responsive.makeResponsive(line);
        layout.addComponent(line);
        layout.setExpandRatio(line,layoutRatio);
        
        return line;
    }
}
