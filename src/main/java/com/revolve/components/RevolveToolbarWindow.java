package com.revolve.components;

import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window;
import com.vaadin.ui.VerticalLayout;
//import org.vaadin.toolbarwindow.ToolbarWindow;

public class RevolveToolbarWindow extends Window {

    private Panel mainpanel;
    private VerticalLayout mainpanelLayout = new VerticalLayout();
    private float defaultWidth;
    private float defaultHeight;
    private float currentWidth;
    private float currentHeight;
    private Component component;
    private int windowCount;

    public RevolveToolbarWindow(String caption) {
        super(caption);
        this.addStyleName("noshadow");
        this.setImmediate(true);
        mainpanel = new Panel();
        mainpanel.setImmediate(true);
    }

    public int getWindowCount() {
        return windowCount;
    }

    public void setWindowCount(int windowCount) {
        this.windowCount = windowCount;
    }


    public void addComponent(Component component) {
        this.component = component;
        mainpanelLayout.addComponent(this.component);
        super.setContent(mainpanelLayout);

        mainpanel.setSizeFull();
        this.component.setSizeFull();
        mainpanel.requestRepaint();
    }

    /**
     * @return component it removes panel from the window and then it returns it
     */
    public Component getDettachedComponent() {
        ((ComponentContainer) this.component.getParent()).removeComponent(this.component);
        return this.component;

    }

    public void setMainComponentId(String componentDebugId) {
        this.component.setId(componentDebugId);
    }

    public float getDefaultWidth() {
        return defaultWidth;
    }

    public void setDefaultWidth(float defaultWidth) {
        this.defaultWidth = defaultWidth;
    }

    public float getDefaultHeight() {
        return defaultHeight;
    }

    public void setDefaultHeight(float defaultHeight) {
        this.defaultHeight = defaultHeight;
    }

    public float getCurrentWidth() {
        return currentWidth;
    }

    public void setCurrentWidth(float curentWidth) {

        this.currentWidth = curentWidth;
        this.setWidth(currentWidth + "px");

    }

    public float getCurrentHeight() {
        return currentHeight;
    }

    public void setCurrentHeight(float currentHeight) {

        this.currentHeight = currentHeight;
        this.setHeight(currentHeight+"px");
    }

}

