package com.revolve.components;

import com.vaadin.data.Item;
import com.vaadin.event.MouseEvents;
import com.vaadin.server.FileResource;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Embedded;
import java.io.File;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.HorizontalLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class RevolveComboBox extends ComboBox {

    public AbsoluteLayout getComboBox(ComboBox cb, boolean list, String listClass, boolean addNew, String addNewClass) {
        AbsoluteLayout abs = new AbsoluteLayout();
        abs.setHeight("75px");
        abs.setWidth("160px");
        HorizontalLayout h = new HorizontalLayout();
        h.setHeight("18px");
        h.setWidth("35px");

        BufferedImage bufferedImage = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR_PRE);
        Font myFont = new Font("Sans", Font.PLAIN, 13);
        Graphics graphics = bufferedImage.getGraphics();
        FontMetrics metrics = graphics.getFontMetrics(myFont);
        int width = metrics.charsWidth(cb.getCaption().toCharArray(), 0, cb.getCaption().length());

        abs.addComponent(cb, "left:0px;top:20px;");
        abs.addComponent(h, "left:" + Integer.toString(width + 8) + "px;top:3px");

        if (addNew == true) {
            Embedded newImg = new Embedded(null, new ThemeResource("icons/plus.png"));
            newImg.setStyleName("image-hand");
            newImg.addListener(new MouseEvents.ClickListener() {
                @Override
                public void click(MouseEvents.ClickEvent event) {
                }
            });
            newImg.setHeight("10px");
            newImg.setWidth("11px");
            h.addComponent(newImg);
        }

        if (list == true) {
            Embedded listImg = new Embedded(null, new ThemeResource("images/popup.png"));
            listImg.setImmediate(true);
            listImg.setStyleName("image-hand");
            listImg.addListener(new MouseEvents.ClickListener() {
                @Override
                public void click(MouseEvents.ClickEvent event) {
                }
            });
            listImg.setWidth("12px");
            listImg.setHeight("12px");
            h.addComponent(listImg);
        }

        return abs;
    }

}
