package com.revolve.components;

import java.util.Iterator;
import java.util.ArrayList;
import fi.jasoft.dragdroplayouts.drophandlers.AbstractDefaultLayoutDropHandler;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.AbsoluteLayout.ComponentPosition;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Table;
import com.vaadin.ui.Button;
import com.vaadin.ui.Accordion;
import fi.jasoft.dragdroplayouts.DDAbsoluteLayout;
import fi.jasoft.dragdroplayouts.details.AbsoluteLayoutTargetDetails;
import fi.jasoft.dragdroplayouts.events.LayoutBoundTransferable;

@SuppressWarnings("serial")
public class RevolveAbsoluteLayoutDropHandler extends
        AbstractDefaultLayoutDropHandler {

    int top = 5;
    int newLine = 55;
    int left = 20;

    @Override
    protected void handleComponentReordering(DragAndDropEvent event) {
        AbsoluteLayoutTargetDetails details = (AbsoluteLayoutTargetDetails) event
                .getTargetDetails();
        DDAbsoluteLayout layout = (DDAbsoluteLayout) details.getTarget();
        LayoutBoundTransferable transferable = (LayoutBoundTransferable) event
                .getTransferable();
        Component component = transferable.getComponent();

        // Get top-left pixel position
        int leftPixelPosition = details.getRelativeLeft();
        int topPixelPosition = details.getRelativeTop();

        Iterator it = layout.iterator();
        ArrayList<Integer> tpos = new ArrayList<Integer>();
        ArrayList<Integer> lpos = new ArrayList<Integer>();
        ArrayList<Integer> wid = new ArrayList<Integer>();
        while (it.hasNext()) {
            Component c = (Component) it.next();
            try {
                ComponentPosition position = layout.getPosition(c);
                int l = (int) position.getLeftValue().floatValue();
                int t = (int) position.getTopValue().floatValue();
                wid.add((int) c.getWidth());
                lpos.add(l);
                tpos.add(t);
            } catch (Exception e) {
            }
        }
        
        ComponentPosition position = layout.getPosition(component);
        position.setTop((float) topPixelPosition, Sizeable.UNITS_PIXELS);
        position.setLeft((float) leftPixelPosition, Sizeable.UNITS_PIXELS);

        /**
         * set the description showing the coordinates
         */
        setCoords(component, leftPixelPosition, topPixelPosition);
    }

    /**
     * Handle a drop from another layout
     *
     * @param event The drag and drop event
     */
    @Override
    protected void handleDropFromLayout(DragAndDropEvent event) {
        AbsoluteLayoutTargetDetails details = (AbsoluteLayoutTargetDetails) event
                .getTargetDetails();
        LayoutBoundTransferable transferable = (LayoutBoundTransferable) event
                .getTransferable();
        Component component = transferable.getComponent();
        Component source = event.getTransferable().getSourceComponent();
        DDAbsoluteLayout layout = (DDAbsoluteLayout) details.getTarget();
        int leftPixelPosition = details.getRelativeLeft();
        int topPixelPosition = details.getRelativeTop();

        // Check that we are not dragging an outer layout into an
        // inner
        // layout
        Component parent = source.getParent();
        while (parent != null) {
            parent = parent.getParent();
        }

        String left = "";
        String top = "";
        // remove component from source
        if (source instanceof ComponentContainer) {
            ComponentContainer sourceLayout = (ComponentContainer) source;
            DDAbsoluteLayout dd = (DDAbsoluteLayout) sourceLayout;
            try {
                ComponentPosition cp = dd.getPosition(component);
                left = Float.toString(cp.getLeftValue());
                top = Float.toString(cp.getTopValue());
            } catch (Exception e) {
            }
            sourceLayout.removeComponent(component);
        }

        // Add component to absolute layout
        try {
            layout.addComponent(component, "left:" + leftPixelPosition + "px;top:"
                    + topPixelPosition + "px");
            setCoords(component, leftPixelPosition, topPixelPosition);
        } catch (Exception e) {
            ComponentContainer sourceLayout = (ComponentContainer) source;
            DDAbsoluteLayout dd = (DDAbsoluteLayout) sourceLayout;
            dd.addComponent(component, "left:" + left + "px;top:" + top + "px");
            // om.messages.displayMessage(om, "AP0144");
        }
    }

    void setCoords(Component c, int left, int top) {
        if (c.getClass().toString().equals("class com.vaadin.ui.Accordion")) {
            Accordion field = (Accordion) c;
            field.setDescription("left:" + Integer.toString(left) + "px;top:" + Integer.toString(top) + "px");
        }
        if (c.getClass().toString().equals("class com.vaadin.ui.TextField")) {
            TextField field = (TextField) c;
            field.setDescription("left:" + Integer.toString(left) + "px;top:" + Integer.toString(top) + "px");
        }
        if (c.getClass().toString().equals("class com.vaadin.ui.TabSheet")) {
            TabSheet field = (TabSheet) c;
            field.setDescription("left:" + Integer.toString(left) + "px;top:" + Integer.toString(top) + "px");
        }
        if (c.getClass().toString().equals("class com.vaadin.ui.Panel")) {
            Panel field = (Panel) c;
            field.setDescription("left:" + Integer.toString(left) + "px;top:" + Integer.toString(top) + "px");
        }
        if (c.getClass().toString().equals("class com.vaadin.ui.ComboBox")) {
            ComboBox field = (ComboBox) c;
            field.setDescription("left:" + Integer.toString(left) + "px;top:" + Integer.toString(top) + "px");
        }
        if (c.getClass().toString().equals("class com.vaadin.ui.CheckBox")) {
            CheckBox field = (CheckBox) c;
            field.setDescription("left:" + Integer.toString(left) + "px;top:" + Integer.toString(top) + "px");
        }
        if (c.getClass().toString().equals("class com.vaadin.ui.Table")) {
            Table field = (Table) c;
            field.setDescription("left:" + Integer.toString(left) + "px;top:" + Integer.toString(top) + "px");
        }
        if (c.getClass().toString().equals("class com.vaadin.ui.Button")) {
            Button field = (Button) c;
            field.setDescription("left:" + Integer.toString(left) + "px;top:" + Integer.toString(top) + "px");
        }
    }
}
