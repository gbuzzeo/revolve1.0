package com.revolve.eventbus;

public class EventTransaction {
    private String eventNumber = "";
    private String eventSource = "";
    
    public EventTransaction(String eventNumber, String eventSource) {
        this.eventNumber = eventNumber;
        this.eventSource = eventSource;
    }
     
    private String getEventNumber() {
        return eventNumber;
    }
    
    private String getEventSource() {
        return eventSource;
    }    
    
}
