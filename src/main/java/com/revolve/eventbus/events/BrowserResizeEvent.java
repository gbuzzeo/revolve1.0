package com.revolve.eventbus.events;

import com.revolve.events.resize.ManageWindows;

public class BrowserResizeEvent {

    ManageWindows manageWindows = new ManageWindows();

    public BrowserResizeEvent() {
        manageWindows.adjustWindows(true);
    }
}
