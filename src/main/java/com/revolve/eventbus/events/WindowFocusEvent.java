package com.revolve.eventbus.events;

import com.vaadin.ui.Window;

public class WindowFocusEvent {
    Window window;
    
    public WindowFocusEvent(Window win) {
        this.window = win;
    }
    
    public Window getWindow() {
        return window;
    }
}