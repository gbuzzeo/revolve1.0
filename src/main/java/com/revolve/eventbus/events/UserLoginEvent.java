package com.revolve.eventbus.events;

import java.util.Iterator;
import com.revolve.transactions.templates.UserLogin;
import org.json.JSONObject;
import com.revolve.RevolveUI;
import com.revolve.client.ClientID;
import com.revolve.eventbus.EventTransaction;
import com.revolve.eventbus.RevolveEventBus;
import com.vaadin.server.VaadinSession;
import com.revolve.eventbus.events.DisplayMessageEvent;
import com.revolve.eventbus.EventID;
import com.revolve.eventbus.EventPublisher;

public class UserLoginEvent {

    public UserLoginEvent(final String userName, final String password) {
        String results = "";
        ClientID clientID = new ClientID();

        /**
         * Get the UserLogin template, load with username and password and send
         * to server to validate
         */
        try {
            UserLogin userLogin = new UserLogin();
            JSONObject ul = userLogin.getObject(userName, password);
            JSONObject received = RevolveUI.jsonServices.getRecord(ul);
            System.out.println(received.toString());
            results = received.getString("RESULTS");
            JSONObject params = received.getJSONObject("PARAMS");
            Iterator it = params.keys();
            while (it.hasNext()) {
                JSONObject num = params.getJSONObject(it.next().toString());
                Iterator numit = num.keys();
                while (numit.hasNext()) {
                    String key = numit.next().toString();
                    if (key.equals("User")) {
                        JSONObject us = num.getJSONObject(key);
                        clientID.setClientId(us.getString("ClientId"));
                        clientID.setUserId(us.getString("UserId"));
                        clientID.setNickname(us.getString("Nickname"));
                    }
                }
            }

        } catch (Exception ex) {
            /**
             * Send a message the login was invalid
             */
            //eventPublisher.displayMessageEvent("AP0004");
            RevolveEventBus.post(new DisplayMessageEvent("AP0004"));
            ex.printStackTrace();
        }

        /**
         * Update the session with the returned values for user
         */
        VaadinSession.getCurrent().setAttribute("Client", clientID);

        /**
         * If login is successful we must remove all the components on the main
         * layout and load the client System Frame Template
         */
        if (results.equals("TRUE")) {
            EventPublisher.removeSplashPageEvent();
            EventPublisher.loadSystemFrameHeaderEvent();
            EventPublisher.windowSizingEvent();
         }

        /**
         * If login is unsuccessful we must send a message to the user
         */
        if (results.equals("FALSE")) {
            RevolveEventBus.post(new DisplayMessageEvent("AP0004"));
        }

    }

}
