package com.revolve.eventbus.events;

import com.revolve.components.RevolveAbsoluteLayoutDropHandler;
import fi.jasoft.dragdroplayouts.DDAbsoluteLayout;
import com.revolve.objects.View;
import com.revolve.objects.Fields;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.DateField;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Table;
import com.vaadin.ui.Label;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.AbsoluteLayout;
import com.revolve.db.RevolveDatabase;
import fi.jasoft.dragdroplayouts.client.ui.LayoutDragMode;
import com.vaadin.data.util.IndexedContainer;
import com.revolve.main.ManageViewContainer;

public class LoadViewLayoutEvent {
    private RevolveDatabase db = new RevolveDatabase();
    private DDAbsoluteLayout layout = new DDAbsoluteLayout();
    private View view;
    private IndexedContainer ic;
    private boolean design;
    private ManageViewContainer manageViewContainer = new ManageViewContainer();

    public LoadViewLayoutEvent(String viewName, IndexedContainer ic, boolean design) {
        /**
         * we need boolean for design as we use same program to load view in designer
         * and live application. If is is designer we donot need to load the
         * manageviewcontainer
         */
        
        this.ic = ic;
        this.design = design;
    
        /**
         * Get the view from the database
         */
        view = db.getView(viewName);
        
        /**
         * Get the fields with a "designerLayout" parent as they are top level
         */
        for (int a = 0; a < view.getFields().size(); a++) {
            Fields field = view.getFields().get(a);
            if (field.getFieldParent().equals("designerLayout")) {
                if (field.getFieldClass().equals("class com.vaadin.ui.Accordion")) {
                    loadAccordion(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.Panel")) {
                    loadPanel(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.Button")) {
                    loadButton(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.CheckBox")) {
                    loadCheckBox(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.ComboBox")) {
                    loadComboBox(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.DateField")) {
                    loadDateField(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.Label")) {
                    loadLabel(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.TabSheet")) {
                    loadTabSheet(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.Table")) {
                    loadTable(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.TextArea")) {
                    loadTextArea(field, layout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.TextField")) {
                    loadTextField(field, layout);
                }
            }
        }
    }

    /**
     * Load the Panels
     */
    void loadPanel(Fields f, DDAbsoluteLayout abs) {
        Panel p = new Panel(f.getFieldCaption());
        DDAbsoluteLayout panelLayout = new DDAbsoluteLayout();
        panelLayout.setId(f.getFieldId());
        panelLayout.setDropHandler(new RevolveAbsoluteLayoutDropHandler());
        p.setId(f.getFieldId());
        p.setHeight(f.getFieldHeight() + "px");
        p.setWidth(f.getFieldWidth() + "px");
        p.setContent(panelLayout);
        abs.addComponent(p, f.getFieldCss());
        loadChildren(f.getFieldId(), panelLayout);
    }

    /**
     * Load the TextFields
     */
    void loadTextField(Fields f, DDAbsoluteLayout abs) {
        TextField tf = new TextField(f.getFieldCaption());
        if(design==false) {
            tf = (TextField) manageViewContainer.loadItem(tf, f.getFieldObject(),f.getFieldName(), "", f.getFieldRequired());
        }
        tf.setId(f.getFieldId());
        tf.setHeight(f.getFieldHeight() + "px");
        tf.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(tf, f.getFieldCss());
    }

    /**
     * Load the TextAreas
     */
    void loadTextArea(Fields f, DDAbsoluteLayout abs) {
        TextArea ta = new TextArea(f.getFieldCaption());
        ta.setId(f.getFieldId());
        ta.setHeight(f.getFieldHeight() + "px");
        ta.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(ta, f.getFieldCss());
    }

    /**
     * Load the ComboBoxes
     */
    void loadComboBox(Fields f, DDAbsoluteLayout abs) {
        ComboBox cb = new ComboBox(f.getFieldCaption());
        cb.setId(f.getFieldId());
        cb.setHeight(f.getFieldHeight() + "px");
        cb.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(cb, f.getFieldCss());
    }

    /**
     * Load the DateFields
     */
    void loadDateField(Fields f, DDAbsoluteLayout abs) {
        DateField df = new DateField(f.getFieldCaption());
        df.setId(f.getFieldId());
        df.setHeight(f.getFieldHeight() + "px");
        df.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(df, f.getFieldCss());
    }

    /**
     * Load the CheckBoxes
     */
    void loadCheckBox(Fields f, DDAbsoluteLayout abs) {
        CheckBox cb = new CheckBox(f.getFieldCaption());
        cb.setId(f.getFieldId());
        cb.setHeight(f.getFieldHeight() + "px");
        cb.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(cb, f.getFieldCss());
    }

    /**
     * Load the Tables
     */
    void loadTable(Fields f, DDAbsoluteLayout abs) {
        Table table = new Table(f.getFieldCaption());
        table.setId(f.getFieldId());
        table.setHeight(f.getFieldHeight() + "px");
        table.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(table, f.getFieldCss());
    }

    /**
     * Load the Buttons
     */
    void loadButton(Fields f, DDAbsoluteLayout abs) {
        Button button = new Button(f.getFieldCaption());
        button.setId(f.getFieldId());
        button.setHeight(f.getFieldHeight() + "px");
        button.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(button, f.getFieldCss());
    }

    /**
     * Load the Labels
     */
    void loadLabel(Fields f, DDAbsoluteLayout abs) {
        Label label = new Label(f.getFieldCaption());
        label.setId(f.getFieldId());
        label.setHeight(f.getFieldHeight() + "px");
        label.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(label, f.getFieldCss());
    }
    
    /**
     * Load the Accordion and Tab Layouts
     */
    void loadTabLayout(Fields f, DDAbsoluteLayout abs) {
        
    }

    /**
     * Load the Accordions
     */
    void loadAccordion(Fields f, DDAbsoluteLayout abs) {
        Accordion accordion = new Accordion();
        String[] tabs = f.getFieldTabs().split("\\|");
        for (int b = 0; b < tabs.length; b++) {
            DDAbsoluteLayout ab = new DDAbsoluteLayout();
            ab.setDragMode(LayoutDragMode.CLONE);
            for(int g=0;g<view.getFields().size();g++){
                Fields field = view.getFields().get(g);
                if(field.getFieldClass().equals("class fi.jasoft.dragdroplayouts.DDAbsoluteLayout")) {
                    String[] idn = field.getFieldId().split("\\|");
                    if(idn[0].equals(tabs[b])) {
                        ab.setId(field.getFieldId()); 
                    }
                }
            }
            ab.setDropHandler(new RevolveAbsoluteLayoutDropHandler());
            accordion.addTab(ab, tabs[b]);
            loadChildren(ab.getId(), ab);
        }
        accordion.setHeight(f.getFieldHeight() + "px");
        accordion.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(accordion, f.getFieldCss());
    }

    /**
     * Load the TabSheets
     */
    void loadTabSheet(Fields f, DDAbsoluteLayout abs) {
        TabSheet tabsheet = new TabSheet();
        String[] tabs = f.getFieldTabs().split("\\|");
        for (int b = 0; b < tabs.length - 1; b++) {
            DDAbsoluteLayout ab = new DDAbsoluteLayout();
            ab.setDragMode(LayoutDragMode.CLONE);
            ab.setId(tabs[b] + "|" + f.getFieldId());
            ab.setDropHandler(new RevolveAbsoluteLayoutDropHandler());
            tabsheet.addTab(ab, tabs[b]);
            loadChildren(f.getFieldId(), ab);
        }
        tabsheet.setHeight(f.getFieldHeight() + "px");
        tabsheet.setWidth(f.getFieldWidth() + "px");
        abs.addComponent(tabsheet, f.getFieldCss());
    }

    void loadChildren(String parent, DDAbsoluteLayout childLayout) {
        for (int a = 0; a < view.getFields().size(); a++) {
            Fields field = view.getFields().get(a);
            if (field.getFieldParent().equals(parent)) {
                if (field.getFieldClass().equals("class com.vaadin.ui.Accordion")) {
                    loadAccordion(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.Panel")) {
                    loadPanel(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.Button")) {
                    loadButton(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.CheckBox")) {
                    loadCheckBox(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.ComboBox")) {
                    loadComboBox(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.DateField")) {
                    loadDateField(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.Label")) {
                    loadLabel(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.TabSheet")) {
                    loadTabSheet(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.Table")) {
                    loadTable(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.TextArea")) {
                    loadTextArea(field, childLayout);
                }
                if (field.getFieldClass().equals("class com.vaadin.ui.TextField")) {
                    loadTextField(field, childLayout);
                }
            }
        }

    }
    
    public AbsoluteLayout getAbsoluteLayout() {
        return (AbsoluteLayout) layout;
    }
    
    public DDAbsoluteLayout getDDAbsoluteLayout() {
        return layout;
    }
}
