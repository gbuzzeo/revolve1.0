package com.revolve.eventbus.events;

import com.revolve.RevolveUI;
import com.vaadin.ui.Window;
import com.vaadin.server.Page;
import java.util.Collection;
import java.util.Iterator;
import org.vaadin.cssinject.CSSInject;

public class WindowSizingEvent {

    int height = 0;
    int width = 0;

    public WindowSizingEvent() {
        /**
         * We must adjust components and fonts on a browser resize on all open windows
         */
        height = Page.getCurrent().getBrowserWindowHeight() - 75;
        width = Page.getCurrent().getBrowserWindowWidth() - 130;
        boolean ok = true;
        if (Page.getCurrent().getBrowserWindowWidth() < 1024) {
            ok = false;
        }
        if (Page.getCurrent().getBrowserWindowHeight() < 768) {
            ok = false;
        }

        setCss();

        /**
         * Get the windows and adjust
         */
        Collection childwindows = RevolveUI.getCurrent().getWindows();
        for (final Iterator cwi = childwindows.iterator(); cwi.hasNext();) {
            final Window child = (Window) cwi.next();
            child.setResizeLazy(false);
            child.setImmediate(true);
            child.setWidth(width + "px");
            child.setHeight(height + "px");
            child.requestRepaint();
            child.setPositionX(105);
            child.setPositionY(65);
        }

    }

    void setCss() {
        /**
         * Adjust various components and fonts
         */
        Float h = new Float(height);
        Float n = new Float(655);
        Float _sz = 13 * (h / n);
        Float _pz = 14 * (h / n);
        int sz = (int) _sz.floatValue();
        int pz = (int) _pz.floatValue();

        Float _fht = 27 * (h / n);
        int fht = (int) _fht.floatValue();

        CSSInject css2 = new CSSInject(RevolveUI.getCurrent().getUI());
        css2.setStyles(".mytheme .v-textfield{font-size:" + Float.toString(sz) + "px; height:" + Float.toString(fht) + "px !important;}");

        CSSInject css2a = new CSSInject(RevolveUI.getCurrent().getUI());
        css2a.setStyles(".mytheme .v-filterselect{font-size:" + Float.toString(sz) + "px; height:" + Float.toString(fht) + "px !important;}");

        CSSInject css2b = new CSSInject(RevolveUI.getCurrent().getUI());
        css2b.setStyles(".mytheme .v-label{font-size:" + Float.toString(sz) + "px; height:" + Float.toString(fht) + "px !important;}");

        CSSInject css2c = new CSSInject(RevolveUI.getCurrent().getUI());
        css2c.setStyles(".mytheme .v-checkbox{font-size:" + Float.toString(sz) + "px; height:" + Float.toString(fht) + "px !important;}");

        //CSSInject css2d = new CSSInject(RevolveUI.getCurrent().getUI());
        //css2d.setStyles(".mytheme .v-menubar{font-size:" + Float.toString(sz) + "px; height:" + Float.toString(fht) + "px !important;}");

        CSSInject css4 = new CSSInject(RevolveUI.getCurrent().getUI());
        css4.setStyles(".mytheme .v-accordion-item-caption  > .v-caption {font-size:" + Float.toString(pz) + "px;}");

        CSSInject css5 = new CSSInject(RevolveUI.getCurrent().getUI());
        css5.setStyles(".mytheme .v-caption{font-size:" + Float.toString(sz) + "px;}");

        Float vh = new Float(height);
        Float oh = new Float(775);
        Float zoom = (vh * 100.f) / oh;

        CSSInject css6 = new CSSInject(RevolveUI.getCurrent().getUI());
        css6.setStyles(".dashboard.v-app select, .dashboard .v-window select{font-size:" + Float.toString(sz) + "px;}");

        CSSInject css7 = new CSSInject(RevolveUI.getCurrent().getUI());
        css7.setStyles(".mytheme .v-table {zoom:" + Float.toString(zoom) + "%;}");

        CSSInject css8 = new CSSInject(RevolveUI.getCurrent().getUI());
        Float _lz = 18 * (h / n);
        int lz = (int) _lz.floatValue();
        css8.setStyles(".mytheme .v-label{font-size:" + Float.toString(sz) + "px;height:" + Float.toString(lz) + "px" + " !important;}");

        CSSInject css9 = new CSSInject(RevolveUI.getCurrent().getUI());
        css9.setStyles(".mytheme .v-panel-caption {zoom:" + Float.toString(zoom) + "%;}");

        CSSInject css10 = new CSSInject(RevolveUI.getCurrent().getUI());
        css10.setStyles(".mytheme .v-accordion-item-caption {zoom:" + Float.toString(zoom) + "%;}");

        CSSInject css11 = new CSSInject(RevolveUI.getCurrent().getUI());
        css11.setStyles(".mytheme .v-embedded-image-hand {zoom:" + Float.toString(zoom) + "%;}");

        CSSInject css12 = new CSSInject(RevolveUI.getCurrent().getUI());
        css12.setStyles(".mytheme .v-button {zoom:" + Float.toString(zoom) + "%;}");

        CSSInject css13 = new CSSInject(RevolveUI.getCurrent().getUI());
        css13.setStyles(".mytheme .v-button-link {zoom:" + Float.toString(zoom) + "%;}");

        System.gc();
        System.gc();
        System.gc();

    }
}
