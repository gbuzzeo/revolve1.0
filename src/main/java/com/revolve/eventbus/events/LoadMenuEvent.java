package com.revolve.eventbus.events;

import com.vaadin.ui.MenuBar;

public class LoadMenuEvent {
    MenuBar menubar = new MenuBar();
 
    public LoadMenuEvent(MenuBar menuBar) {
        this.menubar = menuBar;
    }
    
    public MenuBar getMenuBar() {
        return menubar;
    }
    
}
