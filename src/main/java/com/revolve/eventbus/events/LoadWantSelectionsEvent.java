package com.revolve.eventbus.events;

import java.util.Properties;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Select;
import java.io.InputStream;
import java.util.Enumeration;

public class LoadWantSelectionsEvent {
    private AbsoluteLayout abs = new AbsoluteLayout();
    private Properties properties = new Properties();
    private IndexedContainer wantOptions = new IndexedContainer();
    private HorizontalLayout mainwantLine = new HorizontalLayout();
    private HorizontalLayout mainfl = new HorizontalLayout();
    private ComboBox mainwantTo = new ComboBox();
    private Button maindoit = new Button("Do it");
     
    public LoadWantSelectionsEvent() {
        /**
         * create the want combobox and doit button
         */
        abs.setHeight("40px");
        abs.setWidth("430px");
        
        /**
         * Create the container for the want items from a properties file
         * Note: Needs to be in database at some point
         */
        createContainer();
                
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("../../RevolveWant.properties");
        try {
            properties.load(inputStream);
            Enumeration em = properties.keys();
            while (em.hasMoreElements()) {
                String str = (String) em.nextElement();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * Load the want container
         */
        Enumeration e = properties.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String data[] = properties.getProperty(key).split("\\|");
            Item newItem = wantOptions.getItem(wantOptions.addItem());
            newItem.getItemProperty("Description").setValue(data[0]);
            newItem.getItemProperty("Parent").setValue(data[1]);
            newItem.getItemProperty("Menu").setValue(data[2]);
        }

        /**
         *  Place the want components in the layout
         */
        mainfl.setHeight("40px");
        mainfl.setWidth("367px");
        mainfl.setMargin(true);
        AbsoluteLayout textForm = new AbsoluteLayout();
        textForm.setHeight("40px");
        textForm.setWidth("400px");
        mainwantTo.setContainerDataSource(wantOptions);
        mainwantTo.setFilteringMode(ComboBox.FILTERINGMODE_CONTAINS);
        mainwantTo.setItemCaptionMode(Select.ITEM_CAPTION_MODE_ITEM);
        mainwantTo.setItemCaptionPropertyId("Description");
        mainwantTo.setImmediate(true);
        mainwantTo.setInputPrompt("I want to...");
        mainwantTo.addStyleName("white");
        textForm.addComponent(mainwantTo, "left:25px;bottom:3px;");
        mainwantLine.addComponent(textForm);
        mainwantTo.setWidth("200px");
        mainwantLine.setComponentAlignment(textForm, Alignment.BOTTOM_LEFT);
        maindoit.setHeight("28px");
        textForm.addComponent(maindoit, "left:237px;bottom:3px;");
        maindoit.setImmediate(true);
        //mainwantLine.setExpandRatio(maindoit,1);
        maindoit.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
            }
        });

        mainwantTo.setReadOnly(false);

        abs.addComponent(mainwantLine, "bottom:0px;left:0px;");
    }
    
    void createContainer() {
        wantOptions.addContainerProperty("Description", String.class, null);
        wantOptions.addContainerProperty("Parent", String.class, null);
        wantOptions.addContainerProperty("Menu", String.class, null);
    }
    
    public AbsoluteLayout getWantLine() {
        return abs;
    }
}
