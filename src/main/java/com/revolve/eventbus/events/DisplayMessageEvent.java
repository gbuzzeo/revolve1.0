package com.revolve.eventbus.events;

public class DisplayMessageEvent {

    private final String messageNo;

    public DisplayMessageEvent(final String messageNo) {
        this.messageNo = messageNo;
    }

    public String getMessageNumber() {
        return messageNo;
    }
}
