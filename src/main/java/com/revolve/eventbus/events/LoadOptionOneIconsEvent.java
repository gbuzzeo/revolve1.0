package com.revolve.eventbus.events;

import com.vaadin.addon.responsive.Responsive;
import com.vaadin.event.MouseEvents;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.revolve.eventbus.EventPublisher;
import com.vaadin.server.VaadinSession;
import java.util.Properties;

public class LoadOptionOneIconsEvent {

    VerticalLayout main = new VerticalLayout();
    HorizontalLayout line = new HorizontalLayout();
    HorizontalLayout line2 = new HorizontalLayout();

    public LoadOptionOneIconsEvent() {
        /**
         * Load two lines into the main layout so we add icons to the lines
         */
        main.setSizeFull();
        main.setMargin(true);
        main.setSpacing(true);
        new Responsive(main);

        new Responsive(line);
        line.setSpacing(true);
        line.setMargin(true);
        main.addComponent(line);
        line.setSizeFull();
        line2 = new HorizontalLayout();
        line2.setSpacing(true);
        line2.setMargin(true);
        main.addComponent(line2);
        line2.setSizeFull();
        new Responsive(line2);

        /**
         * Get the client properties
         */
        Properties clientProperties = (Properties) VaadinSession.getCurrent().getAttribute("ClientProperties");

        /**
         * Get the icons property and load the icons
         */
        String[] info = clientProperties.getProperty("icons").split("\\|");
        Embedded[] icon = new Embedded[info.length];
        for (int a = 0; a < info.length; a++) {
            final String[] data = info[a].split("\\,");
            if (a < 4) {
                icon[a] = new Embedded(null, new ThemeResource(data[1]));
                icon[a].addStyleName("image-hand");
                icon[a].setImmediate(true);
                icon[a].setCaption(data[0]);
                icon[a].setDescription(data[3]);
                icon[a].addClickListener(new MouseEvents.ClickListener() {
                    @Override
                    public void click(MouseEvents.ClickEvent event) {
                        EventPublisher.windowLoadEvent(data[2],data[0]);
                    }
                });
                line.addComponent(icon[a]);
            }
            if (a > 3) {
                icon[a] = new Embedded(null, new ThemeResource(data[1]));
                icon[a].addStyleName("image-hand");
                icon[a].setCaption(data[0]);
                icon[a].setImmediate(true);
                icon[a].setDescription(data[3]);
                icon[a].addClickListener(new MouseEvents.ClickListener() {
                    @Override
                    public void click(MouseEvents.ClickEvent event) {
                        EventPublisher.windowLoadEvent(data[2],data[0]);
                    }
                });
                line2.addComponent(icon[a]);
            }
        }

        main.setExpandRatio(line, 50);
        main.setExpandRatio(line2, 50);

    }
    
    public VerticalLayout getVerticalLayout() {
        return main;
    }

}
