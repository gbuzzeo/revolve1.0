
package com.revolve.eventbus.events;

import com.revolve.events.resize.ManageWindows;
import com.vaadin.addon.responsive.Responsive;
import com.vaadin.ui.Window;

public class WindowPlacementEvent {
    Window window;
    
    public WindowPlacementEvent(Window win) {
        new Responsive(win);
        win.addResizeListener(new Window.ResizeListener() {
            @Override
            public void windowResized(Window.ResizeEvent e) {
                if (e.getWindow().getWidth() < 1000) {
                    e.getWindow().setWidth("1000px");
                    e.getWindow().setHeight("540px");
                }
                if (e.getWindow().getHeight() < 540) {
                    e.getWindow().setWidth("1000px");
                    e.getWindow().setHeight("540px");
                }
                ManageWindows manageWindows = new ManageWindows();
                manageWindows.adjustWindow(e.getWindow());
            }
        });
        win.setResizable(true);
        win.setImmediate(true);
        win.setResizeLazy(false);
        win.setClosable(true);
        win.setPositionX(105);
        win.setPositionY(70);

    }
   
}
