package com.revolve.eventbus.events;

import com.revolve.windowmanagers.WindowManagerOptionOne;
import com.google.common.eventbus.Subscribe;
import com.revolve.eventbus.RevolveEventBus;
import com.revolve.eventbus.EventPublisher;
import com.revolve.eventbus.EventPublisher;

public class WindowManagerSelectionEvent {
    EventPublisher eventPublisher = new EventPublisher();

    public WindowManagerSelectionEvent() {
        /**
         * Register event so we may load the window manager
         */
        RevolveEventBus.register(this);

    }

    public WindowManagerSelectionEvent(String option) {
        /**
         * Load correct window manager based on selected option
         */
        if (option.equals("1")) {
            optionOneSelection();
        }
    }

    /**
     * Option One has window manager on top of view to the right of DOit button
     */
    public void optionOneSelection() {
        WindowManagerOptionOne windowManagerOptionOne = new WindowManagerOptionOne();
        RevolveEventBus.post(windowManagerOptionOne);        
        EventPublisher.loadOptionOneIconsEvent();
    }

    /**
     * Option Two has window manager against the left border on bottom part of
     * frame
     */
    public void optionTwoSelection() {

    }
}
