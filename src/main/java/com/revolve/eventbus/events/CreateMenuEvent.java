package com.revolve.eventbus.events;

import com.revolve.db.RevolveDatabase;
import com.revolve.eventbus.RevolveEventBus;
import com.revolve.objects.View;
import com.vaadin.ui.MenuBar;
import java.util.List;
import com.vaadin.server.ThemeResource;
import com.revolve.eventbus.EventPublisher;

public class CreateMenuEvent {
    RevolveDatabase db;
    MenuBar menubar = new MenuBar();
    MenuBar.MenuItem menuTop;
    MenuBar.MenuItem[] mItems;
    int cnt = 0;
 
    public CreateMenuEvent() {
        
        /**
         * Create the menubar
         */
        menubar.setAutoOpen(false);
        menuTop = menubar.addItem("",new ThemeResource("main_icons/menu.png"), null);
        menubar.setImmediate(true);

        /**
         * Get the views from the database and load the menu
         */
        db = new RevolveDatabase();
        List<View> data = db.getViews();
        mItems =  new MenuBar.MenuItem[data.size()];
        for (int b = 0; b < data.size(); b++) {
            final View v = data.get(b);
            mItems[cnt] = menuTop.addItem(v.getViewCaption(), new MenuBar.Command() {
                    @Override
                    public void menuSelected(MenuBar.MenuItem selectedItem) {
                        /**
                         * When selected fire the load Window Event passing the viewname
                         */
                        EventPublisher.windowLoadEvent(v.getViewName(), v.getViewCaption());
                        //RevolveEventBus.post(new WindowLoadEvent(v.getViewName()));
                    }
                });
           cnt = cnt + 1; 
        }
        
        /**
         * Load the menubar
         */
        EventPublisher.loadMenuEvent(menubar);
        //RevolveEventBus.post(new LoadMenuEvent(menubar));
        
        
    }
    
}