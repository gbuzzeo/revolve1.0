package com.revolve.eventbus.events;

import com.revolve.windowmanagers.WindowManagerOptionOne;
import com.google.common.eventbus.Subscribe;
import com.revolve.eventbus.events.RemoveSplashPageEvent;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Embedded;
import java.util.Properties;
import com.revolve.eventbus.RevolveEventBus;
import com.revolve.eventbus.events.WindowManagerSelectionEvent;
import com.revolve.eventbus.events.LoadWantSelectionsEvent;
import com.vaadin.ui.MenuBar;
import com.revolve.ide.Ide;
import com.revolve.eventbus.EventPublisher;

public class LoadSystemFrameHeaderEvent {
    private AbsoluteLayout header = new AbsoluteLayout();

    public LoadSystemFrameHeaderEvent() {
        RevolveEventBus.register(this);
        /**
         * The Header is approx 100px high and 100% of the width the browser.
         * There are some components which are mandatory and others which are
         * client specific
         *
         * Mandatory Components: 1. Menu Component 2. I Want to combobox and
         * Doit Button 3. Sign-out and Settings Menu on far right
         */

        /**
         * Set the width and height of header. it is always 100% width. Height
         * is dependent on resolution so we must calculate
         */
        header.setStyleName("header");
        header.setWidth("100%");
        int height = 100;
        if (Page.getCurrent().getWebBrowser().getScreenHeight() < 900) {
            float h = ((Page.getCurrent().getWebBrowser().getScreenHeight() * 100.f) / 900) / 100;
            double ht = 100 * h;
            height = (int) ht;
        }
        header.setHeight(Integer.toString(height - 5) + "px");

        /**
         * Load the menu
         */
        EventPublisher.createMenuEvent();
        
        /**
         * Load the I want to and DO-IT button
         */
        EventPublisher.loadWantSelectionsEvent();
        
        /**
         * Get the client properties and determine header
         */
        Properties clientProperties = (Properties) VaadinSession.getCurrent().getAttribute("ClientProperties");

        /**
         * Do we show client logo on top left of header
         */
        boolean showLogo = true;
        if (clientProperties.getProperty("logo").equals("no")) {
            showLogo = false;
        }
        Embedded cLogo = null;
        if (showLogo == true) {
            cLogo = new Embedded(null, new ThemeResource("clientImages/" + clientProperties.getProperty("logo")));
        } else {
            cLogo = new Embedded(null, new ThemeResource("clientImages/revolveLogo.png"));
        }
        cLogo.setWidth("70px");
        header.addComponent(cLogo, "top:10px;left:35px");

        /**
         * Setup the window manager the client selected
         */
        RevolveEventBus.post(new WindowManagerSelectionEvent(clientProperties.getProperty("windowmanager")));

        /**
         * Load the menu on far right hand side for IDE and logout
         */
        final MenuBar menubar = new MenuBar();
        menubar.setAutoOpen(false);
        MenuBar.MenuItem menuTop = menubar.addItem("", new ThemeResource("icons/preferences.png"), null);
        menubar.setImmediate(true);
        header.addComponent(menubar, "right:10px;bottom:32px");

        menuTop.addItem("Revolve Ide", new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                Ide ide = new Ide();
                ide.openIde();
            }
        });
        
        /**
         * Fire the event to Load the toolbox on the left hand side of browser
         */
        EventPublisher.createToolBoxEvent();
    }

    /**
     * Return the header for the application
     * @return 
     */
    public AbsoluteLayout getHeader() {
        return header;
    }

    /**
     * Subscribe changes to window selections
     */
    @Subscribe
    public void windowManagerOptionOne(final WindowManagerOptionOne event) {
        header.addComponent(event.loadComboBox(), "left:612px;top:13px");
    }

    /**
     * Add Menu Component
     * Button
     */
    @Subscribe
    public void loadMenuEvent(final LoadMenuEvent event) {
        try {
            header.addComponent(event.getMenuBar(), "left:160px;bottom:30px;");
        }
        catch(Exception e) {
        }
    }

    /**
     * Add i want to and Do it
     * Button
     */
    @Subscribe
    public void loadWantSelectionsEvent(final LoadWantSelectionsEvent event) {
        try {
            header.addComponent(event.getWantLine(), "left:300px;bottom:30px;");
        }
        catch(Exception e) {
        }
    }

}
