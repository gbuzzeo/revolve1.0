package com.revolve.eventbus.events;

import com.revolve.eventbus.EventPublisher;
import com.revolve.view.ViewController;

public class WindowLoadEvent {
 
    public WindowLoadEvent(final String viewName, final String viewCaption) {
        
        /**
         * Call the View Controller
         */       
        ViewController viewController = new ViewController();
        viewController.getView(viewName,viewCaption);
    }
    
}
