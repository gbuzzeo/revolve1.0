package com.revolve.eventbus.events;

import com.vaadin.event.MouseEvents;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ChameleonTheme;
import org.vaadin.johan.*;
import org.vaadin.johan.Toolbox.ORIENTATION;

public class CreateToolBoxEvent {

    private Toolbox toolbox = new Toolbox();
    private Panel widgetPanel = new Panel();
    private VerticalLayout widgetLayout = new VerticalLayout();
    VerticalLayout toolboxComponent = new VerticalLayout();

    public CreateToolBoxEvent() {
        /**
         * Create the toolbox so that is can fit at 200px below the top and 50
         * below the bottom We need to adjust this on a browser resize
         */
        toolboxComponent.setSizeFull();
        int wh = Page.getCurrent().getBrowserWindowHeight() - 300;
        widgetPanel.setHeight(Integer.toString(wh) + "px");
        widgetPanel.setWidth("146px");
        toolbox.setContent(widgetPanel);
        widgetPanel.setContent(widgetLayout);
        widgetLayout.setSizeFull();
        widgetLayout.setMargin(true);
        toolboxComponent.addComponent(toolbox);
        toolbox.setAnimationTime(300);
        toolbox.setFoldOnClickOnly(false);
        toolbox.setOverflowSize(10);
        toolbox.setToolboxVisible(true);
        
        /**
         * Place a dummy icon pic for now
         */
        Embedded iconPanel = new Embedded(null, new ThemeResource("images/SIDEICONPLACEHOLDERS.png"));
        iconPanel.setImmediate(true);
        iconPanel.setStyleName("image-hand");
        iconPanel.addListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent event) {
                
            }
        });
        
        widgetLayout.addComponent(iconPanel);

    }

    public VerticalLayout getToolboxComponent() {
        return toolboxComponent;
    }

}
