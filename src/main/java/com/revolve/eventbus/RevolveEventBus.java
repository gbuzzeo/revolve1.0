package com.revolve.eventbus;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.revolve.RevolveUI;

/**
 * A simple wrapper for Guava event bus. Defines static convenience methods for
 * relevant actions.
 */
public class RevolveEventBus implements SubscriberExceptionHandler {

    private final EventBus eventBus = new EventBus(this);

    public static void post(final Object event) {
        RevolveUI.getEventbus().eventBus.post(event);
    }

    public static void register(final Object object) {
        RevolveUI.getEventbus().eventBus.register(object);
        //RevolveInit.getCurrent().getUI().
    }

    public static void unregister(final Object object) {
        RevolveUI.getEventbus().eventBus.unregister(object);
    }

    @Override
    public final void handleException(final Throwable exception,
            final SubscriberExceptionContext context) {
        exception.printStackTrace();
    }
}