package com.revolve.eventbus;

import com.revolve.main.CreateMainLayout;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.VerticalLayout;
import com.revolve.eventbus.events.*;
import com.vaadin.ui.MenuBar;
import com.revolve.eventbus.EventTransaction;
import com.vaadin.ui.Window;
import com.vaadin.data.util.IndexedContainer;

public class EventPublisher {

    public static void loadSystemFrameHeaderEvent() {
        RevolveEventBus.post(new LoadSystemFrameHeaderEvent());
    }
    
    public static void removeSplashPageEvent() {
        RevolveEventBus.post(new RemoveSplashPageEvent());
    }
    
    public static void userLoginEvent(String username, String password) {
        RevolveEventBus.post(new UserLoginEvent(username, password));
    }
    
    public static void windowSizingEvent() {
        RevolveEventBus.post(new WindowSizingEvent());
    }
    
    public static void windowLoadEvent(String viewName, String viewCaption) {
        RevolveEventBus.post(new WindowLoadEvent(viewName, viewCaption));
    }
    
    public static void createMenuEvent() {
        RevolveEventBus.post(new CreateMenuEvent());
    }
    
    public static void loadMenuEvent(MenuBar menubar){
        RevolveEventBus.post(new LoadMenuEvent(menubar));
    }
    
    public static void loadWindowManagerSelectionEvent(String option) {
        RevolveEventBus.post(new WindowManagerSelectionEvent(option));
    }
    
    public static void displayMessageEvent(String messageNo) {
        RevolveEventBus.post(new DisplayMessageEvent(messageNo));
    }
    
    public static void browserResizeEvent() {
        RevolveEventBus.post(new BrowserResizeEvent());
    }
    
    public static void loadOptionOneIconsEvent() {
        RevolveEventBus.post(new LoadOptionOneIconsEvent());
    }
 
    public static void loadWantSelectionsEvent() {
        RevolveEventBus.post(new LoadWantSelectionsEvent());
    }
    
    public static void createToolBoxEvent() {
        RevolveEventBus.post(new CreateToolBoxEvent());
    }
    
    public static void loadViewLayoutEvent(String viewName, IndexedContainer ic, boolean design) {
        RevolveEventBus.post(new LoadViewLayoutEvent(viewName,ic,design));
    }
    
    public static void windowPlacementEvent(Window window) {
        RevolveEventBus.post(new WindowPlacementEvent(window));
    }
    
}
