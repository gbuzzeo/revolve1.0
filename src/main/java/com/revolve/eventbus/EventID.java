
package com.revolve.eventbus;

import com.revolve.RevolveUI;

public class EventID {
    
    public static String getID() {
        RevolveUI.eventCounter = RevolveUI.eventCounter + 1;
        return Integer.toString(RevolveUI.eventCounter);
    } 
}
