package com.revolve.ide;

import com.google.common.eventbus.Subscribe;
import com.revolve.eventbus.events.LoadViewLayoutEvent;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import com.revolve.db.RevolveDatabase;
import com.revolve.components.RevolveAbsoluteLayoutDropHandler;
import com.revolve.components.TrashAbsoluteLayoutHandler;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.event.LayoutEvents;
import fi.jasoft.dragdroplayouts.*;
import fi.jasoft.dragdroplayouts.drophandlers.*;
import fi.jasoft.dragdroplayouts.client.ui.LayoutDragMode;
import java.util.Collection;
import java.util.Iterator;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.AbsoluteLayout.ComponentPosition;
import com.vaadin.ui.themes.ChameleonTheme;
import com.revolve.components.RevolveToolbarWindow;
import com.vaadin.event.ItemClickEvent;
import elemental.css.CSSStyleDeclaration;
import com.vaadin.server.Sizeable;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.server.ThemeResource;
import com.revolve.objects.View;
import com.revolve.RevolveUI;
import com.vaadin.ui.*;
import com.revolve.objects.Fields;
import com.vaadin.ui.TabSheet.Tab;
import com.revolve.eventbus.EventPublisher;
import com.revolve.eventbus.events.LoadWantSelectionsEvent;
import com.revolve.eventbus.events.LoadViewLayoutEvent;

public class Ide {

    RevolveDatabase db;
    RevolveToolbarWindow window = new RevolveToolbarWindow("Revolve Ide");
    AbsoluteLayout windowLayout = new AbsoluteLayout();
    Panel tools = new Panel("Components");
    DDVerticalLayout toolsLayout = new DDVerticalLayout();
    Panel designer = new Panel("Design");
    DDAbsoluteLayout designerLayout = new DDAbsoluteLayout();
    MenuBar barmenu = new MenuBar();
    int panelCounter = 0;
    int textFieldCounter = 0;
    int dateFieldCounter = 0;
    int tableCounter = 0;
    int checkBoxCounter = 0;
    int comboBoxCounter = 0;
    int buttonCounter = 0;
    int textAreaCounter = 0;
    int labelCounter = 0;
    int tabCounter = 0;
    int addRecCounter = 0;
    int addListCounter = 0;
    int accordionCounter = 0;
    int listCounter = 0;
    int listSelectCounter = 0;
    Panel trash = new Panel("Trash");
    TextField viewName = new TextField("View Name");
    TextField caption = new TextField("View Caption");
    TextField pack = new TextField("Package");
    TextField width = new TextField("Width");
    ComboBox widthUnit = new ComboBox("Unit");
    TextField height = new TextField("Height");
    ComboBox heightUnit = new ComboBox("Unit");
    TextField trans = new TextField("Transaction");
    CheckBox viewLive = new CheckBox("Live View");
    Window saveWin = new Window("Save View Design");
    VerticalLayout saveWinLayout = new VerticalLayout();
    CheckBox newPanel = new CheckBox("New Panel");
    CheckBox newTable = new CheckBox("New Table");
    CheckBox newTab = new CheckBox("New TabSheet");
    CheckBox newAccordion = new CheckBox("New Accordion");
    CheckBox newComboBox = new CheckBox("New ComboBox");
    CheckBox newCheckBox = new CheckBox("New CheckBox");
    CheckBox newTextField = new CheckBox("New TextField");
    CheckBox newDateField = new CheckBox("New DateField");
    CheckBox newTextArea = new CheckBox("New TextArea");
    CheckBox newLabel = new CheckBox("New Label");
    CheckBox newButton = new CheckBox("New Button");
    CheckBox newListSelect = new CheckBox("New ListSelect");
    CheckBox newRec = new CheckBox("New Add Record Image");
    CheckBox newList = new CheckBox("New Listing Image");
    boolean addingNewView = false;
    boolean saved = false;
    String viewProcess = "";
    ArrayList<Fields> fieldArray = new ArrayList<Fields>();

    public void openIde() {
        db = new RevolveDatabase();
        window.setHeight("700px");
        window.setWidth("1430px");
        window.setResizable(false);
        window.setContent(windowLayout);
        designerLayout.setDragMode(LayoutDragMode.CLONE);
        designerLayout.setId("designerLayout");
        toolsLayout.setMargin(true);
        toolsLayout.setSpacing(true);
        tools.setContent(toolsLayout);
        designerLayout.setDropHandler(new RevolveAbsoluteLayoutDropHandler());

        designerLayout.addListener(new LayoutEvents.LayoutClickListener() {
            @Override
            public void layoutClick(LayoutEvents.LayoutClickEvent event) {
                if(event.isShiftKey()) { 
                    changePosition(event.getClickedComponent(),(DDAbsoluteLayout) event.getClickedComponent().getParent());
                }
                if (event.isDoubleClick() == false) {
                     return;
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.TextField")) {
                    modifyTextField(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.DateField")) {
                    modifyDateField(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.Panel")) {
                    modifyPanel(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.ComboBox")) {
                    modifyComboBox(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.CheckBox")) {
                    modifyCheckBox(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.Table")) {
                    modifyTable(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.Button")) {
                    modifyButton(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.TextArea")) {
                    modifyTextArea(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.Label")) {
                    modifyLabel(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.TabSheet")) {
                    modifyTab(event.getClickedComponent());
                }
                if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.Accordion")) {
                    modifyAccordion(event.getClickedComponent());
                }
               if (event.getClickedComponent().getClass().toString().equals("class com.vaadin.ui.ListSelect")) {
                    modifyListSelect(event.getClickedComponent());
                }
            }
        });

        designer.setContent(designerLayout);
        windowLayout.addComponent(tools, "left:10px;top:40px");
        tools.setWidth("250px");
        tools.setHeight("600px");
        windowLayout.addComponent(designer, "left:270px;top:40px");
        designer.setWidth("1150px");
        designer.setHeight("600px");
        loadTools();
        barmenu.setImmediate(true);
        //MenuItem open = barmenu.addItem("Open", null, null);
        //MenuItem save = barmenu.addItem("Save", null, null);
        barmenu.addItem("Open Existing View", new MenuBar.Command() {
            @Override
            public void menuSelected(MenuItem selectedItem) {
                if (viewProcess.equals("opened")) {
                    //messages.displayMessage(om, "AP0143");
                    return;
                }
                designerLayout.removeAllComponents();
                openView();
            }
        });
        barmenu.addItem("New View", new MenuBar.Command() {
            @Override
            public void menuSelected(MenuItem selectedItem) {
                if (viewProcess.equals("opened")) {
                    //messages.displayMessage(om, "AP0143");
                    return;
                }
                viewProcess = "opened";
                newView();
            }
        });
        barmenu.addItem("Save View", new MenuBar.Command() {
            @Override
            public void menuSelected(MenuItem selectedItem) {
                 saveView();
            }
        });

        barmenu.addItem("Close View", new MenuBar.Command() {
            @Override
            public void menuSelected(MenuItem selectedItem) {
                if (viewProcess.equals("opened")) {
                    openAreYouSure();

                } else {
                    designer.setCaption("Designer");
                    setFields(false);
                    designerLayout.removeAllComponents();
                    viewProcess = "closed";
                }
            }
        });

        windowLayout.addComponent(barmenu, "left:10px;top:0px");
        RevolveUI.getCurrent().getUI().addWindow(window);
        window.center();
    }

    void setFields(boolean enabled) {
        newPanel.setEnabled(enabled);
        newTable.setEnabled(enabled);
        newTab.setEnabled(enabled);
        newAccordion.setEnabled(enabled);
        newComboBox.setEnabled(enabled);
        newCheckBox.setEnabled(enabled);
        newTextField.setEnabled(enabled);
        newDateField.setEnabled(enabled);
        newTextArea.setEnabled(enabled);
        newLabel.setEnabled(enabled);
        newButton.setEnabled(enabled);
        newList.setEnabled(enabled);
        newListSelect.setEnabled(enabled);
        newRec.setEnabled(enabled);
      }

    public void openAreYouSure() {
        final Window sure = new Window("Data Not Saved");
        VerticalLayout sureLayout = new VerticalLayout();
        sure.setContent(sureLayout);
        sure.setModal(true);
        //sure.setStyleName(Reindeer.WINDOW_BLACK);
        sure.setWidth("450px");
        sure.setHeight("120px");
        sure.setResizable(false);
        sure.setClosable(false);
        sure.setDraggable(false);
        sure.setCloseShortcut(ShortcutAction.KeyCode.ESCAPE, null);

        Label helpText = new Label(
                "Are you sure you want to close without saving?<br>",
                Label.CONTENT_XHTML);
        sureLayout.addComponent(helpText);
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setMargin(true);
        buttons.setSpacing(true);
        Button yes = new Button("Yes", new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                setFields(false);
                designerLayout.removeAllComponents();
                viewProcess = "closed";
                designer.setCaption("Designer");
                RevolveUI.getCurrent().getUI().removeWindow(sure);
            }
        });
        yes.setImmediate(true);
        yes.setStyleName(Reindeer.BUTTON_DEFAULT);
        yes.focus();
        buttons.addComponent(yes);
        Button no = new Button("No", new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(sure);
            }
        });
        no.setImmediate(true);
        buttons.addComponent(no);

        sureLayout.addComponent(buttons);
        ((VerticalLayout) sure.getContent()).setSpacing(true);

        RevolveUI.getCurrent().getUI().addWindow(sure);
        sure.center();
    }

    void loadTools() {
        newPanel.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newPanel.getValue() == true) {
                    Panel panel = new Panel("New Panel_" + Integer.toString(panelCounter));
                    DDAbsoluteLayout panelLayout = new DDAbsoluteLayout();
                    panelLayout.setId("Panel_" + panelCounter);
                    panelLayout.setDropHandler(new RevolveAbsoluteLayoutDropHandler());
                    panel.setId("Panel_" + Integer.toString(panelCounter));
                    panel.setHeight("200px");
                    panel.setWidth("200px");
                    panel.setContent(panelLayout);
                    designerLayout.addComponent(panel, "bottom:15px;left:10px");
                    panelCounter = panelCounter + 1;
                }
                newPanel.setValue(false);
            }
        });
        newTable.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newTable.getValue() == true) {
                    Table table = new Table("New Table_" + Integer.toString(tableCounter));
                    table.setId("Table_" + Integer.toString(tableCounter));
                    table.setWidth("200px");
                    table.setHeight("200px");
                    designerLayout.addComponent(table, "bottom:15px;left:10px");
                    tableCounter = tableCounter + 1;
                }
                newTable.setValue(false);
            }
        });
        
        newRec.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newRec.getValue() == true) {
                    Embedded addRec = new Embedded(null, new ThemeResource("icons/plus_sign.png"));
                    addRec.setId("AddRec_" + Integer.toString(addRecCounter));
                    designerLayout.addComponent(addRec, "bottom:15px;left:10px");
                    addRecCounter = addRecCounter + 1;
                }
                newRec.setValue(false);
            }
        });

        newList.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newList.getValue() == true) {
                    Embedded addList = new Embedded(null, new ThemeResource("icons/popup.png"));
                    addList.setId("AddList_" + Integer.toString(addListCounter));
                    designerLayout.addComponent(addList, "bottom:15px;left:10px");
                    addListCounter = addListCounter + 1;
                }
                newList.setValue(false);
            }
        });

        newTab.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newTab.getValue() == true) {
                    TabSheet tab = new TabSheet();
                    tab.setId("Tab_" + Integer.toString(tabCounter));
                    tab.setWidth("200px");
                    tab.setHeight("200px");
                    designerLayout.addComponent(tab, "bottom:15px;left:10px");
                    tabCounter = tabCounter + 1;
                    modifyTab(tab);
                }
                newTab.setValue(false);
            }
        });

        newAccordion.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newAccordion.getValue() == true) {
                    Accordion tabs = new Accordion();
                    tabs.setId("Tab_" + Integer.toString(accordionCounter));
                    tabs.setWidth("200px");
                    tabs.setHeight("200px");
                    designerLayout.addComponent(tabs, "bottom:15px;left:10px");
                    accordionCounter = accordionCounter + 1;
                    modifyAccordion(tabs);
                }
                newAccordion.setValue(false);
            }
        });

        newComboBox.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newComboBox.getValue() == true) {
                    ComboBox comboBox = new ComboBox("New ComboBox_" + Integer.toString(comboBoxCounter));
                    comboBox.setWidth("100px");
                    comboBox.setHeight("37px");
                    comboBox.setId("ComboBox_" + Integer.toString(comboBoxCounter));
                    designerLayout.addComponent(comboBox, "bottom:15px;left:10px");
                    comboBoxCounter = comboBoxCounter + 1;
                }
                newComboBox.setValue(false);
            }
        });

        newCheckBox.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newCheckBox.getValue() == true) {
                    CheckBox checkBox = new CheckBox("New CheckBox_" + Integer.toString(checkBoxCounter));
                    checkBox.setWidth("200px");
                    checkBox.setHeight("19px");
                    checkBox.setId("CheckBox_" + Integer.toString(checkBoxCounter));
                    designerLayout.addComponent(checkBox, "bottom:15px;left:10px");
                    checkBoxCounter = checkBoxCounter + 1;
                }
                newCheckBox.setValue(false);
            }
        });

        newTextField.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newTextField.getValue() == true) {
                    TextField textField = new TextField("New TextField_" + Integer.toString(textFieldCounter));
                    textField.setWidth("200px");
                    textField.setHeight("37px");
                    textField.setId("TextField_" + Integer.toString(textFieldCounter));
                    designerLayout.addComponent(textField, "bottom:15px;left:10px");
                    textFieldCounter = textFieldCounter + 1;
                }
                newTextField.setValue(false);
            }
        });

        newDateField.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newDateField.getValue() == true) {
                    DateField textField = new DateField("New DateField_" + Integer.toString(dateFieldCounter));
                    textField.setWidth("200px");
                    textField.setHeight("37px");
                    textField.setId("DateField_" + Integer.toString(dateFieldCounter));
                    designerLayout.addComponent(textField, "bottom:15px;left:10px");
                    dateFieldCounter = dateFieldCounter + 1;
                }
                newDateField.setValue(false);
            }
        });

        newTextArea.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newTextArea.getValue() == true) {
                    TextArea textField = new TextArea("New TextArea" + Integer.toString(textAreaCounter));
                    textField.setWidth("200px");
                    textField.setHeight("100px");
                    textField.setId("TextArea_" + Integer.toString(textAreaCounter));
                    designerLayout.addComponent(textField, "bottom:15px;left:10px");
                    textAreaCounter = textAreaCounter + 1;
                }
                newTextArea.setValue(false);
            }
        });

        newLabel.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newLabel.getValue() == true) {
                    Label textField = new Label("New Label_" + Integer.toString(labelCounter));
                    textField.setWidth("200px");
                    textField.setHeight("37px");
                    textField.setId("Label_" + Integer.toString(labelCounter));
                    designerLayout.addComponent(textField, "bottom:15px;left:10px");
                    labelCounter = labelCounter + 1;
                }
                newLabel.setValue(false);
            }
        });

        newButton.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newButton.getValue() == true) {
                    Button button = new Button("New Button_" + Integer.toString(buttonCounter));
                    button.setWidth("100px");
                    button.setHeight("37px");
                    button.setId("Button_" + Integer.toString(buttonCounter));
                    designerLayout.addComponent(button, "bottom:15px;left:10px");
                    buttonCounter = buttonCounter + 1;
                }
                newButton.setValue(false);
            }
        });
        
        newListSelect.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (newListSelect.getValue() == true) {
                    ListSelect listSelect = new ListSelect("New ListSelect_" + Integer.toString(listSelectCounter));
                    listSelect.setWidth("200px");
                    listSelect.setHeight("200px");
                    listSelect.setId("ListSelect_" + Integer.toString(listSelectCounter));
                    designerLayout.addComponent(listSelect, "bottom:15px;left:10px");
                    listSelectCounter = listSelectCounter + 1;
                }
                newListSelect.setValue(false);
            }
        });

        toolsLayout.addComponent(newTab);
        toolsLayout.addComponent(newPanel);
        toolsLayout.addComponent(newTextField);
        toolsLayout.addComponent(newDateField);
        toolsLayout.addComponent(newComboBox);
        toolsLayout.addComponent(newCheckBox);
        toolsLayout.addComponent(newTable);
        toolsLayout.addComponent(newButton);
        toolsLayout.addComponent(newTextArea);
        toolsLayout.addComponent(newLabel);
        toolsLayout.addComponent(newAccordion);
        //toolsLayout.addComponent(newListSelect);
        //toolsLayout.addComponent(newRec);
        //toolsLayout.addComponent(newList);

        DDAbsoluteLayout trashLayout = new DDAbsoluteLayout();
        trashLayout.setDropHandler(new TrashAbsoluteLayoutHandler());
        trash.setWidth("180px");
        trash.setHeight("120px");
        trash.setContent(trashLayout);
        toolsLayout.addComponent(trash);
        toolsLayout.setComponentAlignment(trash, Alignment.BOTTOM_CENTER);

        setFields(false);
    }
    
    void modifyListSelect(Component c) {
        final ListSelect field = (ListSelect) c;
        final Window win = new Window("ListSelect Design");
        win.setResizable(false);
        win.setHeight("400px");
        win.setWidth("400px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField caption = new TextField("Caption");
        //final TextField object = new TextField("Object");
        final TextField fieldName = new TextField("List Name");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
       // widthUnit.addItem("em");
       // widthUnit.addItem("rem");
       // widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());

        fieldName.setValue(field.getId());
        caption.setValue(field.getCaption());
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                field.setCaption(caption.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(fieldName.getValue().toString());
                //field.setDescription(field.getId());
                //field.setRequired(required.getValue());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        winLayout.addComponent(fieldName);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        winLayout.addComponent(heightLine);
        //winLayout.addComponent(object);
        //winLayout.addComponent(required);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyTextField(Component c) {
        final TextField field = (TextField) c;
        final Window win = new Window("TextField Design");
        win.setResizable(false);
        win.setHeight("520px");
        win.setWidth("420px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField caption = new TextField("Caption");
        final TextField object = new TextField("Object");
        final TextField fieldName = new TextField("Field Name");
        final CheckBox required = new CheckBox("Required");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());

        fieldName.setValue(field.getId());
        caption.setValue(field.getCaption());
        String[] info = new String[2];
        try {
            info = field.getId().split("\\|");
            object.setValue(info[0]);
            fieldName.setValue(info[1]);
        } catch (Exception e) {
        }
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                boolean ok = true;
                if(object.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(field.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(caption.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(ok==false) {
                    EventPublisher.displayMessageEvent("AP0027");
                    return;
                }
                field.setCaption(caption.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(object + "_" + fieldName.getValue().toString());
                //field.setDescription(field.getId());
                field.setRequired(required.getValue());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        HorizontalLayout fieldLine = new HorizontalLayout();
        fieldLine.setSpacing(true);
        fieldLine.addComponent(object);
        fieldLine.addComponent(fieldName);
        object.setRequired(true);
        fieldName.setRequired(true);
        winLayout.addComponent(fieldLine);
        caption.setRequired(true);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        winLayout.addComponent(heightLine);
        //winLayout.addComponent(object);
        winLayout.addComponent(required);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    
    void modifyDateField(Component c) {
        final DateField field = (DateField) c;
        final Window win = new Window("DateField Design");
        win.setResizable(false);
        win.setHeight("400px");
        win.setWidth("400px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField caption = new TextField("Caption");
        final TextField object = new TextField("Object");
        final TextField fieldName = new TextField("Field Name");
        final CheckBox required = new CheckBox("Required");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());

        fieldName.setValue(field.getId());
        caption.setValue(field.getCaption());
        String[] info = new String[2];
        try {
            info = field.getId().split("\\|");
            object.setValue(info[0]);
            fieldName.setValue(info[1]);
        } catch (Exception e) {
        }
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
              boolean ok = true;
                if(object.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(field.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(caption.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(ok==false) {
                    EventPublisher.displayMessageEvent("AP0027");
                    return;
                }
                field.setCaption(caption.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(object.getValue().toString() + "_" + fieldName.getValue().toString());
                //field.setDescription(field.getId());
                field.setRequired(required.getValue());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        HorizontalLayout fieldLine = new HorizontalLayout();
        fieldLine.setSpacing(true);
        fieldLine.addComponent(object);
        fieldLine.addComponent(fieldName);
        winLayout.addComponent(fieldLine);
        object.setRequired(true);
        fieldName.setRequired(true);
        caption.setRequired(true);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        //winLayout.addComponent(heightLine);
        winLayout.addComponent(object);
        winLayout.addComponent(required);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyLabel(Component c) {
        final Label field = (Label) c;
        final Window win = new Window("Label Design");
        win.setResizable(false);
        win.setHeight("400px");
        win.setWidth("400px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField caption = new TextField("Caption");
        final TextField fieldName = new TextField("Field Name");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());

        fieldName.setValue(field.getId());
        caption.setValue(field.getCaption());
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                field.setCaption(caption.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(fieldName.getValue().toString().replaceAll("\\s+", ""));
                //field.setDescription(field.getId());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
               RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        winLayout.addComponent(fieldName);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        //winLayout.addComponent(heightLine);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyTextArea(Component c) {
        final TextArea field = (TextArea) c;
        final Window win = new Window("TextArea Design");
        win.setResizable(false);
        win.setHeight("470px");
        win.setWidth("400px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField caption = new TextField("Caption");
        final TextField object = new TextField("Object");
        final TextField fieldName = new TextField("Field Name");
        final CheckBox required = new CheckBox("Required");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());

        fieldName.setValue(field.getId());
        caption.setValue(field.getCaption());
        String[] info = new String[2];
        try {
            info = field.getId().split("\\|");
            object.setValue(info[0]);
            fieldName.setValue(info[1]);
        } catch (Exception e) {
        }
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                boolean ok = true;
                if(object.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(field.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(caption.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(ok==false) {
                    EventPublisher.displayMessageEvent("AP0027");
                    return;
                }
                field.setCaption(caption.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(object.getValue().toString() + "_" + fieldName.getValue().toString());
                //field.setDescription(field.getId());
                field.setRequired(required.getValue());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        HorizontalLayout fieldLine = new HorizontalLayout();
        fieldLine.setSpacing(true);
        fieldLine.addComponent(object);
        fieldLine.addComponent(fieldName);
        object.setRequired(true);
        fieldName.setRequired(true);
        winLayout.addComponent(fieldLine);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        winLayout.addComponent(heightLine);
        winLayout.addComponent(object);
        winLayout.addComponent(required);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyButton(Component c) {
        final Button field = (Button) c;
        final Window win = new Window("Button Design");
        win.setResizable(false);
        win.setHeight("400px");
        win.setWidth("400px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField caption = new TextField("Caption");
        final TextField fieldName = new TextField("Button Name");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        final ComboBox buttonEvent = new ComboBox("Button Event");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());

        fieldName.setValue(field.getId());
        caption.setValue(field.getCaption());
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                field.setCaption(caption.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(fieldName.getValue().toString().replaceAll("\\s+", ""));
                field.setData(buttonEvent.getValue().toString());
                //field.setDescription(field.getId());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        winLayout.addComponent(fieldName);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        //winLayout.addComponent(heightLine);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyComboBox(Component c) {
        final ComboBox field = (ComboBox) c;
        field.setNewItemsAllowed(true);
        final Window win = new Window("ComboBox Design");
        win.setResizable(false);
        win.setHeight("500px");
        win.setWidth("420px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField caption = new TextField("Caption");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        final TextField fieldEvent = new TextField("Value Loading Event");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());
        final TextField object = new TextField("Object");
        final TextField fieldName = new TextField("Field Name");
        final CheckBox required = new CheckBox("Required");
        fieldName.setValue(field.getId());
        caption.setValue(field.getCaption());
        width.setValue(Float.toString(field.getWidth()));
        height.setValue(Float.toString(field.getHeight()));
        String[] info = new String[2];
        try {
            info = field.getId().split("\\|");
            object.setValue(info[0]);
            fieldName.setValue(info[1]);
        } catch (Exception e) {
        }
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
              boolean ok = true;
                if(object.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(field.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(caption.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(ok==false) {
                    EventPublisher.displayMessageEvent("AP0027");
                    return;
                }
                field.setCaption(caption.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(object.getValue().toString() + "_" + fieldName.getValue());
                field.setData(fieldEvent.getValue().toString());
                //field.setDescription(field.getId());
                field.setRequired(required.getValue());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        HorizontalLayout fieldLine = new HorizontalLayout();
        fieldLine.setSpacing(true);
        fieldLine.addComponent(object);
        fieldLine.addComponent(fieldName);
        winLayout.addComponent(fieldLine);
        object.setRequired(true);
        fieldName.setRequired(true);
        caption.setRequired(true);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
       // winLayout.addComponent(heightLine);
        winLayout.addComponent(fieldEvent);
        winLayout.addComponent(required);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyPanel(Component c) {
        final Panel field = (Panel) c;
        final Window win = new Window("Panel Design");
        win.setResizable(false);
        win.setHeight("380px");
        win.setWidth("400px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField fieldName = new TextField("Panel Name");
        final TextField caption = new TextField("Caption");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());
        caption.setValue(field.getCaption());
        width.setValue(Float.toString(field.getWidth()));
        height.setValue(Float.toString(field.getHeight()));
        fieldName.setValue(field.getId());
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                field.setCaption(caption.getValue().toString());
                field.setId(fieldName.getValue().toString().replaceAll("\\s+", ""));
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        winLayout.addComponent(fieldName);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        winLayout.addComponent(heightLine);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyCheckBox(Component c) {
        final CheckBox field = (CheckBox) c;
        final Window win = new Window("CheckBox Design");
        win.setResizable(false);
        win.setHeight("450px");
        win.setWidth("430px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField fieldObject = new TextField("Field Object");
        final TextField fieldName = new TextField("Field Name");
        final TextField caption = new TextField("Caption");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());
        caption.setValue(field.getCaption());
        width.setValue(Float.toString(field.getWidth()));
        height.setValue(Float.toString(field.getHeight()));
        fieldName.setValue(field.getId());
        final CheckBox required = new CheckBox("Required");
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                boolean ok = true;
                if(fieldObject.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(field.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(caption.getValue().toString().isEmpty()==true) {
                    ok = false;
                }
                if(ok==false) {
                    EventPublisher.displayMessageEvent("AP0027");
                    return;
                }
                field.setCaption(caption.getValue().toString());
                field.setId(fieldObject.getValue().toString() + "_" + fieldName.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        HorizontalLayout fieldLine = new HorizontalLayout();
        fieldLine.setSpacing(true);
        fieldLine.addComponent(fieldObject);
        fieldLine.addComponent(fieldName);
        fieldObject.setRequired(true);
        fieldName.setRequired(true);
        caption.setRequired(true);
        winLayout.addComponent(fieldLine);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        winLayout.addComponent(heightLine);
        winLayout.addComponent(required);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyTable(Component c) {
        final Table field = (Table) c;
        final Window win = new Window("Table Design");
        win.setResizable(false);
        win.setHeight("400px");
        win.setWidth("420px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField fieldName = new TextField("Table Name");
        String[] id = field.getId().split("\\|");
        fieldName.setValue(id[0]);
        final TextField caption = new TextField("Caption");
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());
        final TextField trans = new TextField("Transaction");
        try {
            trans.setValue(id[1]);
        }
        catch(Exception e) {
        }
        caption.setValue(field.getCaption());
        width.setValue(Float.toString(field.getWidth()));
        height.setValue(Float.toString(field.getHeight()));
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                field.setCaption(caption.getValue().toString());
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(fieldName.getValue().toString() + "|" + trans.getValue().toString());
                //field.setDescription(field.getId());
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        winLayout.addComponent(fieldName);
        winLayout.addComponent(caption);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        winLayout.addComponent(heightLine);
        winLayout.addComponent(trans);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyTab(Component c) {
        final TabSheet field = (TabSheet) c;
        final Window win = new Window("TabSheet Design");
        win.setResizable(false);
        win.setHeight("500px");
        win.setWidth("400px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField fieldName = new TextField("Tab Name");
        fieldName.setValue(field.getId());
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        final TextArea tabNames = new TextArea("Tab Names");
        tabNames.setInputPrompt("Seperate names with comma");
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");
        tabNames.setWidth("200px");
        tabNames.setHeight("150px");
        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        height.setValue(Float.toString(field.getHeight()));
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                String[] tn = tabNames.getValue().toString().split("\\,");
                for (int c = 0; c < tn.length; c++) {
                    DDAbsoluteLayout ab = new DDAbsoluteLayout();
                    ab.setDragMode(LayoutDragMode.CLONE);
                    ab.setId(field.getId());
                    ab.setDropHandler(new RevolveAbsoluteLayoutDropHandler());
                    field.addTab(ab, tn[c]);
                }
                field.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                field.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                field.setId(fieldName.getValue().toString().replaceAll("\\s+", ""));
                //field.setDescription(field.getId());
               
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        winLayout.addComponent(tabNames);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        winLayout.addComponent(heightLine);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void modifyAccordion(Component c) {
        final Accordion field = (Accordion) c;
        final Window win = new Window("Accordion Design");
        win.setResizable(false);
        win.setHeight("520px");
        win.setWidth("420px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField fieldName = new TextField("Accordion Name");
        fieldName.setValue(field.getId());
        winLayout.addComponent(fieldName);
        final TextField width = new TextField("Width");
        final ComboBox widthUnit = new ComboBox("Unit");
        final TextField height = new TextField("Height");
        Iterator<Component> i = field.iterator();
        final ComboBox heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");
        final TextArea tabNames = new TextArea("Tab Names");
        tabNames.setInputPrompt("Seperate names with comma");
        tabNames.setWidth("200px");
        tabNames.setHeight("150px");
        String tn = "";
        int cnt = 0;
        final CheckBox[] delTab = new CheckBox[field.getComponentCount()];
        final TextField[] tabNam = new TextField[field.getComponentCount()];
        
        while (i.hasNext()) {
            if(cnt>0) {
                tn = tn + ",";
            }
            Component cc = (Component) i.next();
            Tab tab = field.getTab(cc);
            HorizontalLayout line = new HorizontalLayout();
            line.setSpacing(true);
            delTab[cnt] = new CheckBox("Delete");
            tabNam[cnt] = new TextField("Tab Name");
            tabNam[cnt].setValue(tab.getCaption());
            line.addComponent(tabNam[cnt]);
            line.addComponent(delTab[cnt]);
            line.setComponentAlignment(delTab[cnt], Alignment.BOTTOM_LEFT);
             winLayout.addComponent(line);
            tn = tn + tab.getCaption();
            cnt = cnt + 1;
        }
        tabNames.setValue(tn);

        height.setValue(Float.toString(field.getHeight()));
        heightUnit.setValue(field.getHeightUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        widthUnit.setValue(field.getWidthUnits().toString());
        width.setValue(Float.toString(field.getWidth()));
        height.setValue(Float.toString(field.getHeight()));
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Accordion holdTab = new Accordion();
                String[] tn = tabNames.getValue().toString().split("\\,");
                for (int c = 0; c < tn.length; c++) {
                    if(c<delTab.length) {
                        if(delTab[c].getValue()==true) {
                            continue;
                        }else{
                            holdTab.addTab(field.getTab(c).getComponent(),field.getTab(c).getCaption());
                            c = c + 1;
                            continue;
                        }
                    }
                    DDAbsoluteLayout ab = new DDAbsoluteLayout();
                    ab.setDragMode(LayoutDragMode.CLONE);
                    ab.setId(tn[c] + "|" + field.getId());
                    ab.setDropHandler(new RevolveAbsoluteLayoutDropHandler());
                    holdTab.addTab(ab, tn[c]);
                }
                
                holdTab.setWidth(width.getValue().toString() + widthUnit.getValue().toString());
                holdTab.setHeight(height.getValue().toString() + heightUnit.getValue().toString());
                holdTab.setId(fieldName.getValue().toString().replaceAll("\\s+", ""));
                AbsoluteLayout abs = (AbsoluteLayout) field.getParent();
                abs.replaceComponent(field, holdTab);
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        winLayout.addComponent(tabNames);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        winLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        winLayout.addComponent(heightLine);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }

    void getTabComponents(HasComponents root) {
        Iterator<Component> iterate = root.iterator();
        while (iterate.hasNext()) {
            Component c = iterate.next();
            if (c instanceof HasComponents) {
                getTabComponents((HasComponents) c);
            }
            Fields fields = new Fields();
            fields.setFieldClass(c.getClass().toString());
            fields.setFieldCaption(c.getCaption());
            fields.setFieldWidth(Float.toString(c.getWidth()));
            fields.setFieldHeight(Float.toString(c.getHeight()));
            try {
                if(c.getParent().getClass().toString().equals("class fi.jasoft.dragdroplayouts.DDAbsoluteLayout")) {
                    AbsoluteLayout abs = (AbsoluteLayout) root;
                    ComponentPosition cp = abs.getPosition(c);
                    fields.setFieldCss(cp.getCSSString());
                }
            }
            catch(Exception e) {
            }
            fields.setFieldParent(c.getParent().getId());
            fields.setFieldId(c.getId());
            fieldArray.add(fields);
            System.out.println("Caption:" + c.getCaption() + " / " +
                    "Id:" + c.getId() + " / " +
                    "Parent:" + c.getParent().getId() + " / " +
                    "Class:" + c.getClass().toString() + " / " +
                    "CSS:" + fields.getFieldCss());  
        }        
    }
    
    void getComponents(HasComponents root) {
        Iterator<Component> iterate = root.iterator();
        while (iterate.hasNext()) {
            Component c = iterate.next();
            if (c instanceof HasComponents) {
                getComponents((HasComponents) c);
            }
            Fields fields = new Fields();
            fields.setFieldClass(c.getClass().toString());
            fields.setFieldCaption(c.getCaption());
            fields.setFieldWidth(Float.toString(c.getWidth()));
            fields.setFieldHeight(Float.toString(c.getHeight()));
            try {
                if(c.getParent().getClass().toString().equals("class fi.jasoft.dragdroplayouts.DDAbsoluteLayout")) {
                    AbsoluteLayout abs = (AbsoluteLayout) root;
                    ComponentPosition cp = abs.getPosition(c);
                    fields.setFieldCss(cp.getCSSString());
                }
            }
            catch(Exception e) {
            }
            if(fields.getFieldClass().equals("class com.vaadin.ui.Accordion")) {
                String tabCaptions = "";
                Accordion acc = (Accordion) c;
                Iterator<Component> i = acc.iterator();
                while (i.hasNext()) {
                    Component ac = (Component) i.next();
                    Tab tab = acc.getTab(ac);
                    AbsoluteLayout abs = (AbsoluteLayout) tab.getComponent();
                    getTabComponents(abs);
                    tabCaptions = tabCaptions + tab.getCaption() + "|";
                }
                fields.setFieldTabs(tabCaptions);
            }
            if(fields.getFieldClass().equals("class com.vaadin.ui.TabSheet")) {
                String tabCaptions = "";
                Accordion acc = (Accordion) c;
                Iterator<Component> i = acc.iterator();
                while (i.hasNext()) {
                    Component ac = (Component) i.next();
                    Tab tab = acc.getTab(ac);
                    AbsoluteLayout abs = (AbsoluteLayout) tab.getComponent();
                    getTabComponents(abs);
                    tabCaptions = tabCaptions + tab.getCaption() + "|";
                }
                fields.setFieldTabs(tabCaptions);
            }
            if(fields.getFieldClass().equals("class com.vaadin.ui.CheckBox")) {
                CheckBox f = (CheckBox) c;
                String[] id = f.getId().split("\\_");
                fields.setFieldObject(id[0]);
                fields.setFieldName(id[1]);
            }
            if(fields.getFieldClass().equals("class com.vaadin.ui.TextField")) {
                TextField f = (TextField) c;
                fields.setFieldRequired(f.isRequired());
                String[] id = f.getId().split("\\_");
                fields.setFieldObject(id[0]);
                fields.setFieldName(id[1]);
            }
            if(fields.getFieldClass().equals("class com.vaadin.ui.ComboBox")) {
                ComboBox f = (ComboBox) c;
                fields.setFieldRequired(f.isRequired());
                String[] id = f.getId().split("\\_");
                fields.setFieldObject(id[0]);
                fields.setFieldName(id[1]);
            }
            if(fields.getFieldClass().equals("class com.vaadin.ui.DateField")) {
                DateField f = (DateField) c;
                fields.setFieldRequired(f.isRequired());
                String[] id = f.getId().split("\\_");
                fields.setFieldObject(id[0]);
                fields.setFieldName(id[1]);
            }
            if(fields.getFieldClass().equals("class com.vaadin.ui.TextArea")) {
                TextArea f = (TextArea) c;
                fields.setFieldRequired(f.isRequired());
                String[] id = f.getId().split("\\_");
                fields.setFieldObject(id[0]);
                fields.setFieldName(id[1]);
            }
            fields.setFieldParent(c.getParent().getId());
            fields.setFieldId(c.getId());
            fieldArray.add(fields);
            System.out.println("Caption:" + c.getCaption() + " / " +
                    "Id:" + c.getId() + " / " +
                    "Parent:" + c.getParent().getId() + " / " +
                    "Class:" + c.getClass().toString() + " / " +
                    "CSS:" + fields.getFieldCss());  
        }
    }
    
    void saveView() {
        fieldArray.clear();;
        getComponents(designerLayout);
        View view = new View();
        view.setFields(fieldArray);
        view.setViewName(viewName.getValue().toString());
        view.setViewCaption(caption.getValue().toString());
        view.setViewHeight(height.getValue().toString());
        view.setViewWidth(width.getValue().toString());
        db.addViews(view);
    }

    
    void newView() {
        saveWin.setHeight("420px");
        saveWin.setWidth("400px");
        saveWin.setModal(true);
        saveWin.setContent(saveWinLayout);
        saveWinLayout.setMargin(true);
        saveWinLayout.setSpacing(true);
        saveWinLayout.removeAllComponents();
        saveWin.setResizable(false);
        viewName = new TextField("View Name");
        caption = new TextField("View Caption");
        pack = new TextField("Package");
        width = new TextField("Width");
        widthUnit = new ComboBox("Unit");
        height = new TextField("Height");
        heightUnit = new ComboBox("Unit");
        widthUnit.addItem("px");
        widthUnit.setNullSelectionAllowed(false);
        //widthUnit.addItem("em");
        //widthUnit.addItem("rem");
        //widthUnit.addItem("ex");
        //widthUnit.addItem("in");
        //widthUnit.addItem("cm");
        //widthUnit.addItem("mm");
        //widthUnit.addItem("pt");
        //widthUnit.addItem("pc");
        heightUnit.addItem("px");
        //heightUnit.addItem("em");
        //heightUnit.addItem("rem");
        //heightUnit.addItem("ex");
        //heightUnit.addItem("in");
        //heightUnit.addItem("cm");
        //heightUnit.addItem("mm");
        //heightUnit.addItem("pt");
        //heightUnit.addItem("pc");
        heightUnit.setNullSelectionAllowed(false);
        heightUnit.setValue("px");
        widthUnit.setValue("px");
        saveWin.setCaption("Saving View");
        width.setValue("1200");
        height.setValue("550");
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
            }
        });

        Button ok = new Button("Done");
        ok.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                designer.setCaption("Designer - " + viewName.getValue().toString());
                setFields(true);
                RevolveUI.getCurrent().getUI().removeWindow(saveWin);
            }
        });

        saveWinLayout.removeComponent(buttons);
        buttons.addComponent(ok);

        saveWinLayout.addComponent(viewName);
        saveWinLayout.addComponent(caption);
        //saveWinLayout.addComponent(pack);
        saveWinLayout.addComponent(trans);
        HorizontalLayout widthLine = new HorizontalLayout();
        widthLine.setSpacing(true);
        widthLine.addComponent(width);
        widthLine.addComponent(widthUnit);
        saveWinLayout.addComponent(widthLine);
        HorizontalLayout heightLine = new HorizontalLayout();
        heightLine.setSpacing(true);
        heightLine.addComponent(height);
        heightLine.addComponent(heightUnit);
        //saveWinLayout.addComponent(heightLine);
        //saveWinLayout.addComponent(viewLive);
        saveWinLayout.addComponent(buttons);
        saveWinLayout.setExpandRatio(buttons, 1);

        RevolveUI.getCurrent().getUI().addWindow(saveWin);

    }

    void openView(Item item) {
        View view = db.getView(item.getItemProperty("View Name").getValue().toString());
        viewName.setValue(view.getViewName());
        height.setValue(view.getViewHeight());
        width.setValue(view.getViewWidth());
        caption.setValue(view.getViewCaption());
        EventPublisher.loadViewLayoutEvent(view.getViewName(),null,true);
    }
       
    void openView() {
        final Window win = new Window("Open View");
        final Table views = new Table();
        views.addStyleName(ChameleonTheme.TABLE_SMALL);
        views.addStyleName(ChameleonTheme.TABLE_STRIPED);
        views.addStyleName(ChameleonTheme.TABLE_BORDERLESS);
        IndexedContainer viewContainer = new IndexedContainer();
        viewContainer.addContainerProperty("View Name", String.class, null);
        //viewContainer.addContainerProperty("View Version", String.class, null);
        viewContainer.addContainerProperty("View Width", String.class, null);
        viewContainer.addContainerProperty("View Height", String.class, null);
        views.setContainerDataSource(viewContainer);
        views.setHeight("400px");
        views.setWidth("500px");
        views.setSelectable(true);
        views.setImmediate(true);
        views.addListener(new ItemClickEvent.ItemClickListener() {
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    viewProcess = "opened";
                    Item item = views.getItem(event.getItemId());
                    openView(item);
                    RevolveUI.getCurrent().getUI().removeWindow(win);
                }
            }
        });

        List<View> data = db.getViews();
        for (int b = 0; b < data.size(); b++) {
            View v = data.get(b);
            Item newItem = viewContainer.getItem(viewContainer.addItem());
            newItem.getItemProperty("View Name").setValue(v.getViewName());
            //newItem.getItemProperty("View Version").setValue(v.getViewVersion());
            newItem.getItemProperty("View Width").setValue(v.getViewWidth());
            newItem.getItemProperty("View Height").setValue(v.getViewHeight());
        }
        win.setHeight("550px");
        win.setWidth("550px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        win.setResizable(false);
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                viewProcess = "saved";
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                viewProcess = "closed";
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        //buttons.addComponent(save);
        buttons.addComponent(close);
       // buttons.setExpandRatio(close, 1);
        winLayout.addComponent(views);
        //views.setWidth("100px");
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
    }
  
    Component findByClass(HasComponents root, String className) {
        Iterator<Component> iterate = root.iterator();
        while (iterate.hasNext()) {
            Component c = iterate.next();
            if (className.equals(c.getClass().toString())) {
                return c;
            }
            if (c instanceof HasComponents) {
                Component cc = findByClass((HasComponents) c, className);
            }
        }

        return null;
    }
    
    void changePosition(final Component component, DDAbsoluteLayout layout) {
        final Window win = new Window("Change Position");
        win.setResizable(false);
        win.setHeight("200px");
        win.setWidth("200px");
        win.setModal(true);
        VerticalLayout winLayout = new VerticalLayout();
        win.setContent(winLayout);
        winLayout.setMargin(true);
        winLayout.setSpacing(true);
        final TextField leftPos = new TextField("Left Position");
        final TextField topPos = new TextField("Top Position");
        final ComponentPosition cp = layout.getPosition(component);
        leftPos.setValue(Float.toString(cp.getLeftValue()));
        topPos.setValue(Float.toString(cp.getTopValue()));
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        buttons.setHeight("45px");
        Button save = new Button("Save");
        save.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                cp.setLeftValue(Float.parseFloat(leftPos.getValue().toString()));
                cp.setTopValue(Float.parseFloat(topPos.getValue().toString()));
                setCoords(component,Float.parseFloat(leftPos.getValue().toString()),Float.parseFloat(topPos.getValue().toString()));
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        Button close = new Button("Close");
        close.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RevolveUI.getCurrent().getUI().removeWindow(win);
            }
        });

        buttons.addComponent(save);
        buttons.addComponent(close);
        buttons.setExpandRatio(close, 1);
        winLayout.addComponent(leftPos);
        winLayout.addComponent(topPos);
        winLayout.addComponent(buttons);
        winLayout.setExpandRatio(buttons, 1);
        RevolveUI.getCurrent().getUI().addWindow(win);
        win.center();
         
    }
    
   void setCoords(Component c, float left, float top) {
        if(c.getClass().toString().equals("class com.vaadin.ui.Accordion")) {
            Accordion field =(Accordion) c;
            field.setDescription("left:" + Float.toString(left) + "px;top:" + Float.toString(top) + "px");
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.TextField")) {
            TextField field =(TextField) c;
            field.setDescription("left:" + Float.toString(left) + "px;top:" + Float.toString(top) + "px");
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.TabSheet")) {
            TabSheet field =(TabSheet) c;
            field.setDescription("left:" + Float.toString(left) + "px;top:" + Float.toString(top) + "px");
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.Panel")) {
            Panel field =(Panel) c;
            field.setDescription("left:" + Float.toString(left) + "px;top:" + Float.toString(top) + "px");
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.ComboBox")) {
            ComboBox field =(ComboBox) c;
            field.setDescription("left:" + Float.toString(left) + "px;top:" + Float.toString(top) + "px");
        }        
        if(c.getClass().toString().equals("class com.vaadin.ui.CheckBox")) {
            CheckBox field =(CheckBox) c;
            field.setDescription("left:" + Float.toString(left) + "px;top:" + Float.toString(top) + "px");
        }    
        if(c.getClass().toString().equals("class com.vaadin.ui.Table")) {
            Table field =(Table) c;
            field.setDescription("left:" + Float.toString(left) + "px;top:" + Float.toString(top) + "px");
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.Button")) {
            Button field =(Button) c;
            field.setDescription("left:" + Float.toString(left) + "px;top:" + Float.toString(top) + "px");
        }    
    }

    /**
     * Load the View
     */
    @Subscribe
    public void loadViewLayoutEvent(final LoadViewLayoutEvent event) {
        try {
            designerLayout = event.getDDAbsoluteLayout();
        }
        catch(Exception e) {
        }
    }

}
 