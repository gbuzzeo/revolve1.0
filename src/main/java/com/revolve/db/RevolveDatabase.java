package com.revolve.db;

import com.db4o.*;
import com.db4o.cs.*;
import com.db4o.query.Query;
import com.revolve.objects.View;
import com.vaadin.server.VaadinSession;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class RevolveDatabase implements Serializable {
    EntityManagerFactory emf;
    EntityManager em;
    int PORT = 0;
    Properties clientProperties;

    public RevolveDatabase() {
        /**
         * Get the client properties and get the port for the database
         */
        clientProperties = (Properties) VaadinSession.getCurrent().getAttribute("ClientProperties");
        PORT = Integer.parseInt(clientProperties.getProperty("port"));
    }
    
    /**
     * Open the Entity Manager for Persistence
     * ObjectDB is database
     */
    public void openDB(int s) {
         try {
            Map<String, String> properties = new HashMap<String, String>();
            properties.put("javax.persistence.jdbc.user", "admin");
            properties.put("javax.persistence.jdbc.password", "admin");
            emf = Persistence.createEntityManagerFactory("objectdb://localhost:" + PORT + "/" + clientProperties.getProperty("clientid") + ".odb", properties);
            Map<String,Object> prop = new HashMap();
            prop.put("javax.persistence.lock.timeout", 3000);
            em = emf.createEntityManager();
       }
       catch(Exception e) {
           closeDB();
           e.printStackTrace();
       }
    }
     
    /**
     * Close the database
     */
    public void closeDB() {
        try {
            if(em.isOpen()) em.close();
            if(emf.isOpen()) emf.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Add a new View
     * @param View 
     */
    public void addViews(View View) {
        openDB(1);
        boolean foundSw = false;
        List<View> views = null;
        try {
            try {
                views = em.createQuery("SELECT c FROM View c WHERE c.viewName='" + View.getViewName() + "' AND c.viewVersion='" + View.getViewVersion() + "'",View.class).getResultList();
                if(views.size()>0) {
                    em.getTransaction().begin();
                    em.createQuery("DELETE FROM View c WHERE c.viewName='" + View.getViewName() + "' AND c.viewVersion='" + View.getViewVersion() + "'").executeUpdate();
                    em.getTransaction().commit();
                }
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            em.getTransaction().begin();
            em.persist(View);   
            em.getTransaction().commit();
        }
        catch(Exception e) {
        closeDB();
        }
        closeDB();
     }
  
     /**
      * Get an existing view
      * @param viewName
      * @return 
      */
     public View getView(String viewName) {
        openDB(2);
        View view = new View();
        TypedQuery<View> query = em.createQuery("SELECT c FROM View c WHERE viewName='" + viewName + "'", View.class);
        try {
            view = query.getSingleResult();
        }
        catch(Exception e) {
        closeDB();
        return view;
        }
        closeDB();
        return view;
    }
    
    /**
     * Get a list of all views in database
     * @return 
     */
    public List getViews() {
        openDB(3);
        List<View> views = null;
        TypedQuery<View> query = em.createQuery("SELECT c FROM View c", View.class);
        try {
            views = query.getResultList();
        }
        catch(Exception e) {
        closeDB();
        return views;
        }
        closeDB();
        return views;
     }

}
