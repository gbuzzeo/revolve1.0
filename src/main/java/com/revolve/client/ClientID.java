
package com.revolve.client;

public class ClientID {
    private String clientId = "";
    private String sellerID = "134644128185129411";
    private String accountHolderID = "135148347188708914";
    private int clientPort = 4501;
    private String presentationUsername = " fts_presentation";
    private String presentationId = "137322812617274115";
    private String sellerPassword = "test";
    private String version = "1";
    private String userId = "";
    private String username = "";
    private String password = " ";
    private String systemClient = "2";
    private String systemPassword = "test";
    private String nickName = "";
    
    // 1 = free
    // 2 = basic
    private String sessionId = "";
    
    public String getSystemClient() {
        return systemClient;
    }
    public String getSystemPassword() {
        return systemPassword;
    }
    
    public void setSessionId(String SessionId) {
        sessionId = SessionId;
    }
    public String getSessionId() {
        return sessionId;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername() {
        return username;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public String getClientid() {
        return clientId;
    }
  
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserId() {
        return userId;
    }

    public void setUserPassword(String userPassword) {
        this.password = userPassword;
    }
    public String getUserPassword() {
        return password;
    }

    public String getSellerID() {
        return sellerID;
    }
    
   public String getAccountHolderID() {
        return accountHolderID;
    }
   
   public void setClientPort(int port) {
       clientPort = port;
   }
   
   public int getClientPort() {
       return clientPort;
   }
   
   public String getVersion() {
       return version;
   }
   public String getPresentationUsername() {
       return presentationUsername;
   }
   
   public String getSellerPassword() {
       return sellerPassword;
   }
   
   public String getPresentationId() {
       return presentationId;
   }
   
    public void setNickname(String nickName) {
        this.nickName = nickName;
    }

    public String getNickname() {
        return nickName;
    }

}
