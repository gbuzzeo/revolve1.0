package com.revolve.view;

import com.google.common.eventbus.Subscribe;
import java.util.Iterator;
import java.util.Collection;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.Item;
import com.vaadin.ui.Window;
import com.revolve.db.RevolveDatabase;
import com.revolve.eventbus.EventPublisher;
import com.revolve.eventbus.events.LoadViewLayoutEvent;
import com.revolve.main.ManageViewContainer;
import com.revolve.eventbus.RevolveEventBus;
import com.revolve.RevolveUI;
import com.revolve.objects.View;

public class ViewController {
    private String viewName = "";
    private Window window;
    private VerticalLayout main = new VerticalLayout();
    private ManageViewContainer manageViewContainer = new ManageViewContainer();
    private IndexedContainer ic = new IndexedContainer();
    private RevolveDatabase db = new RevolveDatabase();

    public void getView(String viewName, String viewCaption) {
        /**
         * Register with EventBus
         */
        RevolveEventBus.register(this);
        
        /**
         * Create the Window
         */
        window = new Window(viewCaption);
        
        /**
         * Create the view container for the fields on the view
         */
        initializeContainers();         
        ic = manageViewContainer.getContainer();
        
        /**
         * Get the view Layout. Pass the container and inform that this is not the designer
         */
        EventPublisher.loadViewLayoutEvent(viewName,ic,false);
        
        /**
         * Get the view from the database so we can get height and width
         */
        View view = db.getView(viewName);
        window.setHeight(view.getViewHeight() + "px");
        window.setWidth(view.getViewWidth() + "px");
        
        /**
         * Display the window
         */
        RevolveUI.getCurrent().getUI().addWindow(window);
        
        /**
         * Place the window in correct place
         */
        EventPublisher.windowPlacementEvent(window);
    }

    public void initializeContainers() {
        /**
         *Create the shell container for this view
         */
        manageViewContainer = new ManageViewContainer();
        manageViewContainer.buildViewContainer();
        ic = manageViewContainer.getContainer();
    }

    public void processView() {
    }
    
    Boolean checkforRequiredFields() {
        Collection c = manageViewContainer.getContainer().getItemIds();
        Iterator cit = c.iterator();
        boolean ok =true;
        while(cit.hasNext()) {
            Item item = manageViewContainer.getContainer().getItem(cit.next());
            boolean req = (Boolean) item.getItemProperty("FieldRequired").getValue();
            if(req==true) {
                if(item.getItemProperty("Value").getValue().toString().isEmpty()==true) {
                    ok = false;
                }
            }
       }
       return ok;
    }
    
      /**
     * Load the View
     */
    @Subscribe
    public void loadViewLayoutEvent(final LoadViewLayoutEvent event) {
        try {
            window.setContent(event.getAbsoluteLayout());
        }
        catch(Exception e) {
        }
    }

}
