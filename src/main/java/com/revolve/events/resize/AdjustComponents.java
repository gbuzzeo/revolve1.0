
package com.revolve.events.resize;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Component;

public class AdjustComponents {

    public void addComponent(AbsoluteLayout layout, Component c, String css, int w, int h) {
        if(c.getHeight()<1) {
            c.setHeight(25, Sizeable.Unit.PIXELS);
        }
        if(c.getWidth()<1) {
            c.setWidth(150, Sizeable.UNITS_PIXELS);
        }
        String newCss = "";
        String[] mcss = css.split(";");
        for(int a=0;a<mcss.length;a++) {
            newCss = newCss + adjustCss(mcss[a],c,layout,w,h);
        }
        if(c.getClass().toString().equals("class com.vaadin.ui.Embedded")) {
            layout.addComponent(c, newCss);
            return;
        }
        
 //       if(c.getClass().toString().equals("class com.vaadin.ui.Button")) {
 //           layout.addComponent(c, newCss);
 //           return;
 //       }
        
        c.setSizeFull();
        layout.addComponent(c, newCss);
    }
    
     String adjustCss(String css, Component c, AbsoluteLayout layout, int w, int h) {
        String newCss = "";
        String cssStriped = css.replace("px","");
        String[] data = cssStriped.split("\\:");
        if(data[0].equals("left")) {
            Float d = new Float(data[1]);
            Float left = ((d*100.f)/w);
            Float right = 100 - left - ((c.getWidth()*100.f)/w);
            boolean useRight = false;
            newCss = "left:" + left + "%;right:" + right + "%;";
        }
        if(data[0].equals("right")) {
            Float d = new Float(data[1]);
            Float right = ((d*100.f)/w);
            Float left = 100 - right - ((c.getWidth()*100.f)/w);
            newCss = "right:" + right + "%;left:" + left + "%;";
         }
        if(data[0].equals("top")) {
            Float d = new Float(data[1]);
            Float _top = ((d*100.f)/h);
            Float _bottom = 100 - _top - ((c.getHeight()*100.f)/h);
            int bottom = (int) _bottom.floatValue();
            int top = (int) _top.floatValue();
            newCss = "top:" + top + "%;bottom:" + bottom + "%;";
        }
        if(data[0].equals("bottom")) {
            Float d = new Float(data[1]);
            Float _bottom = ((d*100.f)/h);
            Float _top = 100 - _bottom - ((c.getHeight()*100.f)/h);
            boolean useBottom = false;
            int bottom = (int) _bottom.floatValue();
            int top = (int) _top.floatValue();
            newCss = "bottom:" + bottom + "%;top:" + top + "%;";
        }

       return newCss;
    }

}
