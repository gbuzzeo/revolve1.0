
package com.revolve.events.lists;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.ComboBox;
import org.tepi.filtertable.FilterTable;

public class PagingRow {
    HorizontalLayout abs = new HorizontalLayout();
    Button left = new Button("<<");
    Button right = new Button(">>");
    Label pagesLabel = new Label("Total Pages:1");
    ComboBox pageNo = new ComboBox();
    FilterTable table;
    ComboBox recordsPerPage = new ComboBox();
    ComboBox sortDirection = new ComboBox();
     
    public HorizontalLayout getPagingRow(FilterTable _table) {
        table = _table;
        abs.setHeight("30px");
        abs.setSpacing(true);
        
        left.setHeight("28px");
        right.setHeight("28px");
        left.setWidth("50px");
        abs.addComponent(left);
        right.setWidth("50px");
        abs.addComponent(right);
        
        pagesLabel.setWidth("80px");
        //abs.addComponent(pagesLabel);
        //abs.setComponentAlignment(pagesLabel, Alignment.MIDDLE_RIGHT);
               
        pageNo.setNullSelectionAllowed(false);
        pageNo.addItem("page 1");
        pageNo.setValue("page 1");
        
        pageNo.setWidth("125px");
        abs.addComponent(pageNo);
       
        recordsPerPage.setNullSelectionAllowed(false);
        recordsPerPage.addItem("20 records per page");
        recordsPerPage.addItem("50 records per page");
        recordsPerPage.addItem("100 records per page");
        recordsPerPage.addItem("250 records per page");
        recordsPerPage.addItem("500 records per page");
        recordsPerPage.addItem("All records per page");
        recordsPerPage.setValue("20 records per page");
        abs.addComponent(recordsPerPage);
        
        sortDirection.setNullSelectionAllowed(false);
        sortDirection.addItem("ASC");
        sortDirection.setItemCaption("ASC","Ascending Order");
        sortDirection.addItem("DESC");
        sortDirection.setItemCaption("DESC","Descending Order");
        sortDirection.setValue("DESC");
        //abs.addComponent(sortDirection);
        
        abs.setExpandRatio(recordsPerPage,1);

        return abs;
    }
}
