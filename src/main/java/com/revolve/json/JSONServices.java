/*
 * Developer : From the Source LLC (legal@fls.com)
 * All code (c)2012 From the Source, LLC. all rights reserved
 * 
 * Version 1.0
 */
package com.revolve.json;

import com.google.common.eventbus.Subscribe;
import java.util.*;
import java.net.URLDecoder;
import java.net.URL;
import java.net.URLConnection;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import java.net.*;
import java.io.*;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import com.revolve.RevolveUI;
import com.revolve.client.ClientID;
import com.revolve.utils.SystemVariables;
import com.vaadin.server.VaadinSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class JSONServices {
    String results;
    URLConnection urlConn;

    public JSONObject addRecord(JSONObject obj) {
        JSONObject data = new JSONObject();
        JSONObject jsonObjOutput = null;
        String str = null;
        try {
            HttpServletRequest request;
            Calendar calendar = Calendar.getInstance();
  
            System.out.println("CREATE JSON FILE: " + obj.toString());
            URL url;
            DataOutputStream printout;
            DataInputStream input;
            url = new URL("http://" + SystemVariables.daoURL);
            urlConn = url.openConnection();
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            urlConn.setUseCaches(false);
            urlConn.setRequestProperty("Content-Type", "application/json");
            printout = new DataOutputStream(urlConn.getOutputStream());
            String content = obj.toString();
            printout.writeBytes(content);
            printout.flush();
            printout.close();
            input = new DataInputStream(urlConn.getInputStream());

            while (null != ((str = input.readLine()))) {
                try {
                    jsonObjOutput = new JSONObject(str);

                    results = jsonObjOutput.toString();
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
        return jsonObjOutput;
    }

    public JSONObject getRecord(JSONObject obj) {
        Date date1 = new Date();
        JSONObject data = new JSONObject();
        try {
            System.out.println("CREATE JSON FILE: " + obj.toString());
            URL url;
            URLConnection urlConn;
            DataOutputStream printout;
            DataInputStream input;
            url = new URL("http://" + SystemVariables.daoURL);
            urlConn = url.openConnection();
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            urlConn.setUseCaches(false);
            urlConn.setRequestProperty("Content-Type", "application/json");
            printout = new DataOutputStream(urlConn.getOutputStream());
            String content = obj.toString();
            printout.writeBytes(content);
            printout.flush();
            printout.close();
            input = new DataInputStream(urlConn.getInputStream());
            String str;
            JSONObject jsonObjOutput = null;
            while (null != ((str = input.readLine()))) {
                try {
                    jsonObjOutput = new JSONObject(str);
                    data = jsonObjOutput;
                     results = jsonObjOutput.toString();
                } catch (Exception e) {
                    results = e.toString();
                    System.out.println(results);
                }
            }
        } catch (Exception e) {
            results = e.toString();
            e.printStackTrace();
        }
       return data;
    }
          
}
