
package com.revolve.transactions.templates;

import org.json.JSONObject;
import com.revolve.client.ClientID;
import com.vaadin.server.VaadinSession;

public class UserLogin {
    
     public JSONObject getObject(final String username, final String password) {
        /**
         * Create the String for the user login
         */
        JSONObject obj = new JSONObject();
        JSONObject trans = new JSONObject();
        try {
            /**
             * We need to get the clientID from session with defaults
             */
            ClientID clientID = (ClientID) VaadinSession.getCurrent().getAttribute("Client");
            trans.put("client_id",clientID.getSystemClient());
            trans.put("module", "user");
            trans.put("task","user_login");
            trans.put("user_id",clientID.getSellerID());
            trans.put("password",clientID.getSystemPassword());
            trans.put("pl_session", clientID.getSessionId());
            obj.put("TRANS", trans);
            JSONObject params = new JSONObject();
            JSONObject zero = new JSONObject();
            JSONObject user = new JSONObject();
            user.put("UserName",username);
            user.put("Password",password);
            JSONObject xIndex = new JSONObject();
            xIndex.put("User", "user_1");
            user.put("xIndex",xIndex);
            user.put("xName", "User");
            user.put("xParent", "User");
            zero.put("User",user);
            params.put("0", zero);
            obj.put("PARAMS", params);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        return obj;
    }
    
}
