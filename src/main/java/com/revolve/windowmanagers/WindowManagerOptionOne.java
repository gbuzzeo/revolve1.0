package com.revolve.windowmanagers;

import com.vaadin.data.Property;
import com.vaadin.ui.ComboBox;
import java.util.Collection;
import com.vaadin.ui.Window;
import java.util.Iterator;
import com.revolve.RevolveUI;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Embedded;
import com.revolve.eventbus.RevolveEventBus;
import com.vaadin.ui.Alignment;
import com.revolve.eventbus.events.WindowPlacementEvent;
import com.revolve.eventbus.EventPublisher;
import com.vaadin.ui.AbsoluteLayout;

public class WindowManagerOptionOne {  
    private int windowId = 0;
    private ComboBox openWindows = new ComboBox();
    private ThemeResource downIco = new ThemeResource("images/cb_arrow_down.png");
    private ThemeResource upIco = new ThemeResource("images/cb_arrow_down_1.png");
    private Embedded hideShowIcon = new Embedded(null, downIco);
    private AbsoluteLayout managerLayout = new AbsoluteLayout();
     
    public AbsoluteLayout loadComboBox() {
        /**
         * Load the WindowManager ComboBox and Show/Hide Icon on the HorizontalLayout and
         * return the layout to the caller
         */
        openWindows.setImmediate(true);
        openWindows.addListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                Collection childwindows = RevolveUI.getCurrent().getUI().getWindows();
                for (final Iterator cwi = childwindows.iterator(); cwi.hasNext();) {
                    final Window child = (Window) cwi.next();
                    try {
                         if(child.getId().equals(openWindows.getValue().toString())) {
                            WindowPlacementEvent windowPlacementEvent = new WindowPlacementEvent(child);
                            RevolveEventBus.post(windowPlacementEvent);
                            child.focus();
                        }
                        Collection cc = openWindows.getItemIds();
                        openWindows.setCaption("Open Windows: " + Integer.toString(cc.size()));
                        if(openWindows.getValue().toString().isEmpty()==true) {
                            hideShowIcon.setSource(upIco);
                        }
                    }
                    catch(Exception e) {
                    }
                }
            }
        });
        
        managerLayout.setWidth("250px");
        managerLayout.setHeight("50px");
        openWindows.setCaption("Open Windows: " + "0");
        openWindows.setWidth("200px");
        managerLayout.addComponent(openWindows,"left:0px;bottom:0px");
        managerLayout.addComponent(hideShowIcon,"left:210px;bottom:-3px");
        
        return managerLayout;
    }
    
    public int addWindow(Window win) {
        windowId = windowId + 1;
        win.setId(Integer.toString(windowId));
        openWindows.addItem(Integer.toString(windowId));
        openWindows.setItemCaption(Integer.toString(windowId),win.getCaption());
        openWindows.setValue(Integer.toString(windowId));
        Collection cc = openWindows.getItemIds();
        openWindows.setCaption("Open Windows: " + Integer.toString(cc.size()));
 
        return windowId;
    }
    
    public void removeWindow(Window win) {
        openWindows.removeItem(win.getId());
        RevolveUI.getCurrent().getUI().removeWindow(win);
        Collection cc = openWindows.getItemIds();
        openWindows.setCaption("Open Windows: " + Integer.toString(cc.size())); 
        openWindows.setValue(null);
        Collection c = openWindows.getItemIds();
        Iterator cit = c.iterator();
        while (cit.hasNext()) {
            openWindows.setValue(cit.next().toString());
        }
    }
    
    public int getWindowId() {
        return windowId;
    }
    
    public void changeCaption(String windowId, String caption) {
        Collection childwindows = RevolveUI.getCurrent().getUI().getWindows();
        for (final Iterator cwi = childwindows.iterator(); cwi.hasNext();) {
            final Window child = (Window) cwi.next();
            if (child.getId().equals(windowId)) {
                child.setCaption(caption);
                openWindows.setItemCaption(child.getId(), child.getCaption());
            }
        }      
    }
    
    public void setFocus(String cap) {
        Collection c = openWindows.getItemIds();
        Iterator cit = c.iterator();
        while(cit.hasNext()) {
            Object id = cit.next();
            String caption = openWindows.getItemCaption(id);
            if(caption.equals(cap)) {
                openWindows.setValue(id);
            }
        }
        
    }
   
    
    
}
