package com.revolve;

import com.google.common.eventbus.Subscribe;
import com.vaadin.server.Page;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.Page.BrowserWindowResizeListener;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import javax.servlet.annotation.WebServlet;
import com.revolve.eventbus.RevolveEventBus;
import com.revolve.eventbus.events.UserLoginEvent;
import com.vaadin.addon.responsive.Responsive;
import com.revolve.init.*;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedSession;
import com.revolve.client.ClientID;
import com.revolve.main.ApplicationMessages;
import com.vaadin.ui.AbsoluteLayout;
import com.revolve.eventbus.events.BrowserResizeEvent;
import com.revolve.json.JSONServices;
import com.revolve.main.CreateMainLayout;
import com.revolve.main.SplashPage;
import com.revolve.eventbus.EventPublisher;

@Theme("mytheme")
@SuppressWarnings("serial")
public class RevolveUI extends UI {
    private ClientID clientID = new ClientID();
    private final RevolveEventBus revolveEventBus = new RevolveEventBus();
    public static JSONServices jsonServices = new JSONServices();
    CreateMainLayout createMainLayout;
    AbsoluteLayout mainLayout;
    public static int eventCounter = 0;

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = RevolveUI.class, widgetset = "com.revolve.AppWidgetSet")
    public static class Servlet extends VaadinServlet {
    }

    @Override
    public void init(VaadinRequest request) {
        /**
         * Register the Event Bus
         */
        RevolveEventBus.register(this);

        /**
         * Set the mainLayout
         */
        createMainLayout = new CreateMainLayout();
        mainLayout = createMainLayout.getLayout();
        setContent(mainLayout);

        /**
         * Since we are using Valo, make this responsive
         */
        new Responsive(this);

        /**
         * Get Session ID
         */
        VaadinSession vSession = UI.getCurrent().getSession();
        WrappedSession wSession = vSession.getSession();
        String sessionID = wSession.getId();

        /**
         * Load the properties for the client by passing client from the url
         * string
         */
        LoadClientProperties loadClientProperties = new LoadClientProperties();
        loadClientProperties.loadProperties(request.getParameter("client"));

        /**
         * Load Messages
         */
        ApplicationMessages applicationMessages = new ApplicationMessages();
        applicationMessages.loadMessages();

        /**
         * Display login Layout
         */
        LoginLayout loginLayout = new LoginLayout();
        mainLayout.addComponent(loginLayout.getLoginLayout(), "left:590px;top:125px");

        /**
         * Load the Browser resize event
         */
        Page.getCurrent().addBrowserWindowResizeListener(
                new BrowserWindowResizeListener() {
                    @Override
                    public void browserWindowResized(
                            final BrowserWindowResizeEvent event) {       
                                EventPublisher.browserResizeEvent();
                            }
                });

        /**
         * Display the Splash Page graphics
         */
        SplashPage splashPage = new SplashPage(mainLayout);
        
    }

    /**
     * @return An instance for accessing the (dummy) services layer.
     */
    public static RevolveEventBus getEventbus() {
        return ((RevolveUI) getCurrent()).revolveEventBus;
    }

}
